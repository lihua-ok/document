#作者    曹波
#for install what apollo needed library
#LOGfile="install-apollo.log"
echo -e "\033[33m start to install softwares \033[0m"
echo -e "\033[33myellow color is successful flags \033[0m"
echo -e "\033[31mred color is error flags \033[0m"

sudo dpkg -i sogoupinyin_2.1.0.0086_amd64.deb
sudo apt-get install -f

sudo dpkg -i gitkraken-v3.6.4.deb

sudo dpkg -i google-chrome-stable_current_amd64.deb

sudo dpkg -i teamviewer_12.0.85001_i386.deb
sudo apt-get install -f


# update
sudo apt-get update
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get update ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get update failed~! \033[0m"
fi

#build-essential
sudo apt-get install build-essential cmake cmake-qt-gui autoconf automake autopoint libglib2.0-dev libtool openjdk-8-jdk python-dev  libvtk6-dev
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install build-essential cmake cmake-qt-gui ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install build-essential cmake cmake-qt-gui failed~! \033[0m"
	exit 1
fi

#qt5
echo -e "\033[33mstart to install qt5\033[0m"
#sudo apt-get purge qt5*
sudo apt-get install qt5-default qttools5-dev-tools

#地图
echo -e "\033[33mstart to install the map needed\033[0m"

sudo apt-get install gcc make libeigen3-dev libgsl0-dev libproj-dev libann-dev  libboost-dev libboost-filesystem-dev libboost-system-dev libboost-thread-dev libboost-chrono-dev
if [ "$?" -eq "0" ];then
	echo -e "\033[33m  apt-get install gcc make mesa-common-dev  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install gcc make mesa-common-dev failed~! \033[0m"
	exit 1
fi

#opengl
sudo apt-get install libglfw3-dev libglew-dev mesa-common-dev freeglut3-dev libglu1-mesa-dev 
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install libglfw3-dev libglew-dev ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install libglfw3-dev libglew-dev failed~! \033[0m"
	exit 1
fi

#install pcl first
echo -e "\033[33mstart to install the PCL\033[0m"

sudo apt-get install libpcl-dev
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install libpcl-dev  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install libpcl-dev failed~! \033[0m"
	exit 1
fi

#opencv
echo -e "\033[33mstart to install the opencv-3.2.0\033[0m"
tar -zxvf opencv-3.2.0.tar.gz	
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file opencv-3.2.0.tar.gz ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack opencv-3.2.0.tar.gz file failed~! \033[0m"
	exit 1
fi

cd opencv-3.2.0
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd opencv-3.2.0  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd opencv-3.2.0 failed~! \033[0m"
	exit 1
fi
sudo mkdir ./3rdparty/ippicv/downloads
sudo mkdir ./3rdparty/ippicv/downloads/linux-808b791a6eac9ed78d32a7666804320e
sudo cp ../ippicv_linux_20151201.tgz ./3rdparty/ippicv/downloads/linux-808b791a6eac9ed78d32a7666804320e
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cp ../ippicv_linux_20151201.tgz  ok~! \033[0m"
else
	echo -e "\033[31m Error: can not find ippicv_linux_20151201.tgz \033[0m"
	exit 1
fi

mkdir build
cd build
sudo cmake ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake ./  failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi
sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd .. #out opencv-3.2.0
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

#protobuf
echo -e "\033[33mstart to install the protobuf-cpp-3.4.1\033[0m"

unzip protobuf-cpp-3.4.1.zip
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file protobuf-cpp-3.4.1.zip ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file protobuf-cpp-3.4.1.zip failed~! \033[0m"
	exit 1
fi

cd protobuf-3.4.1
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd protobuf-3.4.1  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd protobuf-3.4.1 failed~! \033[0m"
	exit 1
fi

./autogen.sh
if [ "$?" -eq "0" ];then
	echo -e "\033[33m ./autogen.sh  ok~! \033[0m"
else
	echo -e "\033[31m Error: ./autogen.sh failed~! \033[0m"
	exit 1
fi

sudo ./configure
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo ./configure  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo ./configure failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make check
if [ "$?" -eq "0" ];then
	echo -e "\033[33m   ok~! \033[0m"
else
	echo -e "\033[31m Error:  failed~! \033[0m"
	exit 1
fi

sudo make install 
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install   ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install  failed~! \033[0m"
	exit 1
fi

sudo ldconfig # refresh shared library cache 
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo ldconfig   ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo ldconfig  failed~! \033[0m"
	exit 1
fi

sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs  failed~! \033[0m"
	exit 1
fi

sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs failed~! \033[0m"
	exit 1
fi

sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs failed~! \033[0m"
	exit 1
fi

cd .. #out protobuf-cpp-3.4.1
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

#ros-kinetic

echo -e "\033[33mstart to install the ROS-kinetic\033[0m"

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo sh -c  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo sh -c failed~! \033[0m"
	exit 1
fi

sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116 failed~! \033[0m"
	exit 1
fi

sudo apt-get update
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get update  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get update failed~! \033[0m"
	
fi

sudo apt-get install ros-kinetic-desktop-full
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install ros-kinetic-desktop-full  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install ros-kinetic-desktop-full failed~! \033[0m"
	exit 1
fi

echo  "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
if [ "$?" -eq "0" ];then
	echo -e "\033[33m  echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc  ok~! \033[0m"
else
	echo -e "\033[31m Error: echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc failed~! \033[0m"
	exit 1
fi

source ~/.bashrc
if [ "$?" -eq "0" ];then
	echo -e "\033[33m source ~/.bashrc  ok~! \033[0m"
else
	echo -e "\033[31m Error: source ~/.bashrc failed~! \033[0m"
	exit 1
fi

#lcm
echo -e "\033[33mstart to install the LCM-1.3.0\033[0m"

tar -zxvf lcm-1.3.0.tar.gz	
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file lcm-1.3.0.tar.gz ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file failed~! \033[0m"
	exit 1
fi

cd lcm-1.3.0
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd lcm-1.3.0  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd lcm-1.3.0 failed~! \033[0m"
	exit 1
fi
chmod 777 bootstrap.sh
if [ "$?" -eq "0" ];then
	echo -e "\033[33m chmod 777 bootstrap.sh  ok~! \033[0m"
else
	echo -e "\033[31m Error: chmod 777 bootstrap.sh failed~! \033[0m"
	exit 1
fi

./bootstrap.sh
if [ "$?" -eq "0" ];then
	echo -e "\033[33m ./bootstrap.sh  ok~! \033[0m"
else
	echo -e "\033[31m Error: ./bootstrap.sh failed~! \033[0m"
	exit 1
fi

#sudo ./configure
#if [ "$?" -eq "0" ];then
#	echo -e "\033[33m ./configure  ok~! \033[0m"
#else
#	echo -e "\033[31m Error: ./configure failed~! \033[0m"
#	exit 1
#fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

#install glog-master
echo -e "\033[33mstart to install the glog-master\033[0m"
unzip glog-master.zip
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file glog-master.zip ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file glog-master.zip failed~! \033[0m"
	exit 1
fi

cd glog-master
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd glog-master  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd glog-master failed~! \033[0m"
	exit 1
fi

sudo cmake ./
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake ./ failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

echo -e "\033[33mstart to install the curlpp-master\033[0m"
unzip curlpp-master.zip
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file curlpp-master.zip ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file curlpp-master.zip failed~! \033[0m"
	exit 1
fi
#curlpp
cd curlpp-master
if [ "$?" -eq "0" ];then
	echo -e "\033[33m  cd curlpp-master ok~! \033[0m"
else
	echo -e "\033[31m Error: cd curlpp-master failed~! \033[0m"
	exit 1
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m mkdir build  ok~! \033[0m"
else
	echo -e "\033[31m Error: mkdir build failed~! \033[0m"
	exit 1
fi

cd build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd build  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd build failed~! \033[0m"
	exit 1
fi

sudo cmake ./..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cmake ./..  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake ./.. failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd .. #out build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

cd .. #out curlpp-master
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

#gtest
echo -e "\033[33mstart to install the googletest-master\033[0m"
unzip googletest-master.zip
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file googletest-master.zip ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file googletest-master.zip failed~! \033[0m"
	exit 1
fi

cd googletest-master
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd googletest-master  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd googletest-master failed~! \033[0m"
	exit 1
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m mkdir build  ok~! \033[0m"
else
	echo -e "\033[31m Error: mkdir build failed~! \033[0m"
	exit 1
fi

cd build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m mkdir build  ok~! \033[0m"
else
	echo -e "\033[31m Error: mkdir build failed~! \033[0m"
	exit 1
fi

sudo cmake ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m  sudo cmake .. ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake .. failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd .. #out build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

cd .. #out googletest-master
if [ "$?" -eq "0" ];then
	echo -e "\033[33m   ok~! \033[0m"
else
	echo -e "\033[31m Error:  failed~! \033[0m"
	exit 1
fi

#qpOASE
echo -e "\033[33mstart to install the fileqpOASES-3.2.1\033[0m"
unzip qpOASES-3.2.1.zip
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file qpOASES-3.2.1.zip ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file qpOASES-3.2.1.zip failed~! \033[0m"
	exit 1
fi

cd qpOASES-3.2.1
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd qpOASES-3.2.1  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd qpOASES-3.2.1  failed~! \033[0m"
	exit 1
fi
sudo cmake ./
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake ./ failed~! \033[0m"
	exit 1
fi

sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

cd .. 
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

#ipopt
echo -e "\033[33mstart to install the Ipopt-3.12.8.tgz\033[0m"
tar -zxvf Ipopt-3.12.8.tgz	
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file Ipopt-3.12.8.tgz ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack file failed~! \033[0m"
	exit 1
fi

cd Ipopt-3.12.8
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd Ipopt-3.12.8  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd Ipopt-3.12.8 failed~! \033[0m"
	exit 1
fi

./configure
if [ "$?" -eq "0" ];then
	echo -e "\033[33m ./configure  ok~! \033[0m"
else
	echo -e "\033[31m Error: ./configure failed~! \033[0m"
	exit 1
fi
sudo make -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make  ok~! \033[0m"
else
	echo -e "\033[31m Error:  failed~! \033[0m"
	exit 1
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make install  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make install failed~! \033[0m"
	exit 1
fi

sudo cp -r lib/. /usr/local/lib
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp -r lib/. /usr/local/lib  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp -r lib/. /usr/local/lib failed~! \033[0m"
	exit 1
fi

sudo cp -r include/. /usr/local/include
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp -r include/. /usr/local/include  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp -r include/. /usr/local/include failed~! \033[0m"
	exit 1
fi

cd .. #out Ipopt-3.12.8
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  
if [ "$?" -eq "0" ];then
	echo -e "\033[33m export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  ok~! \033[0m"
else
	echo -e "\033[31m Error:　export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  failed~! \033[0m"
	exit 1
fi

sudo ldconfig  
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo ldconfig  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo ldconfig failed~! \033[0m"
	exit 1
fi

#install ccache.
sudo apt-get install  ccache
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install  ccache  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install  ccache failed~! \033[0m"
	exit 1
fi

#install caffe 
echo -e "\033[33mstart to install the caffe dependencies\033[0m"
sudo apt-get install libatlas-base-dev libsnappy-dev libleveldb-dev liblmdb-dev libgflags-dev libhdf5-openmpi-dev
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo apt-get install libatlas-base-dev  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo apt-get install libatlas-base-dev failed~! \033[0m"
	exit 1
fi

echo -e "\033[33mstart to install the caffe and copy to home! \033[0m"
tar -zxvf caffe.tar.gz	
if [ "$?" -eq "0" ];then
	echo -e "\033[33m unpack file caffe.tar.gz ok~! \033[0m"
else
	echo -e "\033[31m Error:unPack caffe.tar.gz failed~! \033[0m"
	exit 1
fi

cd caffe 
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd caffe   ok~! \033[0m"
else
	echo -e "\033[31m Error: cd caffe  failed~! \033[0m"
	exit 1
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m mkdir build  ok~! \033[0m"
else
	echo -e "\033[31m Error: mkdir build failed~! \033[0m"
	exit 1
fi

cd build
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd build  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd build failed~! \033[0m"
	exit 1
fi

sudo cmake ../.
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cmake ../.  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cmake ../. failed~! \033[0m"
	exit 1
fi

sudo make all -j6
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo make all  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo make all failed~! \033[0m"
	exit 1
fi

cd ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi

cd ..
if [ "$?" -eq "0" ];then
	echo -e "\033[33m cd ..  ok~! \033[0m"
else
	echo -e "\033[31m Error: cd .. failed~! \033[0m"
	exit 1
fi


sudo cp -r caffe ~/.
if [ "$?" -eq "0" ];then
	echo -e "\033[33m sudo cp -r caffe ~/.  ok~! \033[0m"
else
	echo -e "\033[31m Error: sudo cp -r caffe ~/. failed~! \033[0m"
	exit 1
fi
