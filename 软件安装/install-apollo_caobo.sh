#作者    曹波
#for install what apollo needed library
#LOGfile="install-apollo.log"
echo "\033[33m if the dependence file is not exist, show the error and write the log to install-apollo.log. But still continue the install \033[0m"
echo "\033[33myellow color is successful flags \033[0m"
echo "\033[31mred color is error flags \033[0m"
#echo "\033[33mif the dependence file is not exist, show the error and write the log to install-apollo.log. But still continue the install \033[0m" > ${LOGfile}
#echo "yellow color is successful flags" > $LOGfile 
#echo "red color  is error flags" >> $LOGfile 
#Do you want to continue? [Y/n]
#Press [ENTER] to continue or ctrl-c to cancel adding it

# update
sudo apt-get update
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get update ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get update failed~! \033[0m"
fi

#cmake
sudo apt-get install build-essential cmake cmake-qt-gui
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install build-essential cmake cmake-qt-gui ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install build-essential cmake cmake-qt-gui failed~! \033[0m"
fi

echo "\033[33mstart to install the map needed\033[0m"

#地图：
sudo apt-get install gcc make mesa-common-dev freeglut3-dev libglu1-mesa-dev libeigen3-dev libgsl0-dev libproj-dev libann-dev  libboost-dev libboost-filesystem-dev libboost-system-dev libboost-thread-dev libboost-chrono-dev
if [ "$?" -eq "0" ];then
	echo "\033[33m  apt-get install gcc make mesa-common-dev  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install gcc make mesa-common-dev failed~! \033[0m"
fi

#echo "\033[33mSuccessful to install the map needed\033[0m"

#caffe
echo "\033[33mstart to install the caffe needed\033[0m"
sudo apt-get install libatlas-base-dev libsnappy-dev libleveldb-dev liblmdb-dev libgflags-dev libhdf5-openmpi-dev
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install libatlas-base-dev  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install libatlas-base-dev failed~! \033[0m"
fi

#echo "\033[33mSuccessful to install the caffe needed\033[0m"

#apollo
sudo apt-get install libglfw3-dev libglew-dev
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install libglfw3-dev libglew-dev ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install libglfw3-dev libglew-dev failed~! \033[0m"
fi

#install pcl first
echo "\033[33mstart to install the PCL\033[0m"

sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl  ok~! \033[0m"
else
	echo "\033[31m Error: sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl failed~! \033[0m"
fi

sudo apt-get update
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get update  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get update failed~! \033[0m"
fi

sudo apt-get install libpcl-dev
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install libpcl-dev  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install libpcl-dev failed~! \033[0m"
fi

#echo "\033[33mSuccessful to install the PCL\033[0m"
#install ros-kinetic

echo "\033[33mstart to install the ROS-kinetic\033[0m"

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo sh -c  ok~! \033[0m"
else
	echo "\033[31m Error: sudo sh -c failed~! \033[0m"
fi

sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116 failed~! \033[0m"
fi

sudo apt-get update
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get update  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get update failed~! \033[0m"
fi

sudo apt-get install ros-kinetic-desktop-full
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install ros-kinetic-desktop-full  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install ros-kinetic-desktop-full failed~! \033[0m"
fi

echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
if [ "$?" -eq "0" ];then
	echo "\033[33m  echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc  ok~! \033[0m"
else
	echo "\033[31m Error: echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc failed~! \033[0m"
fi

source ~/.bashrc
if [ "$?" -eq "0" ];then
	echo "\033[33m source ~/.bashrc  ok~! \033[0m"
else
	echo "\033[31m Error: source ~/.bashrc failed~! \033[0m"
fi

#echo "\033[33mSuccessful to install the ROS-kinetic\033[0m"

#next install dependence library. first check the file is exist or not. 
#if the dependence file is not exist, show the error and write the log to install-apollo.log. But still continue the install.
echo "\033[33mstart to install the LCM-1.3.0\033[0m"
sudo apt-get install build-essential autoconf automake autopoint libglib2.0-dev libtool openjdk-8-jdk python-dev
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install build-essential autoconf automake autopoint libglib2.0-dev libtool openjdk-8-jdk python-dev  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install build-essential autoconf automake autopoint libglib2.0-dev libtool openjdk-8-jdk python-dev failed~! \033[0m"
fi

tar -zxvf lcm-1.3.0.tar.gz	
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file lcm-1.3.0.tar.gz ok~! \033[0m"
else
	echo "\033[31m Error:unPack file failed~! \033[0m"
fi

cd lcm-1.3.0
if [ "$?" -eq "0" ];then
	echo "\033[33m cd lcm-1.3.0  ok~! \033[0m"
else
	echo "\033[31m Error: cd lcm-1.3.0 failed~! \033[0m"
fi
chmod 777 bootstrap.sh
if [ "$?" -eq "0" ];then
	echo "\033[33m chmod 777 bootstrap.sh  ok~! \033[0m"
else
	echo "\033[31m Error: chmod 777 bootstrap.sh failed~! \033[0m"
fi

./bootstrap.sh
if [ "$?" -eq "0" ];then
	echo "\033[33m ./bootstrap.sh  ok~! \033[0m"
else
	echo "\033[31m Error: ./bootstrap.sh failed~! \033[0m"
fi

./configure
if [ "$?" -eq "0" ];then
	echo "\033[33m ./configure  ok~! \033[0m"
else
	echo "\033[31m Error: ./configure failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd ..
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

#install glog-master
echo "\033[33mstart to install the glog-master\033[0m"
unzip glog-master.zip
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file glog-master.zip ok~! \033[0m"
else
	echo "\033[31m Error:unPack file glog-master.zip failed~! \033[0m"
fi

cd glog-master
if [ "$?" -eq "0" ];then
	echo "\033[33m cd glog-master  ok~! \033[0m"
else
	echo "\033[31m Error: cd glog-master failed~! \033[0m"
fi

sudo cmake ./
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake ./ failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd ..
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

echo "\033[33mstart to install the curlpp-master\033[0m"
unzip curlpp-master.zip
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file curlpp-master.zip ok~! \033[0m"
else
	echo "\033[31m Error:unPack file curlpp-master.zip failed~! \033[0m"
fi

cd curlpp-master
if [ "$?" -eq "0" ];then
	echo "\033[33m  cd curlpp-master ok~! \033[0m"
else
	echo "\033[31m Error: cd curlpp-master failed~! \033[0m"
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo "\033[33m mkdir build  ok~! \033[0m"
else
	echo "\033[31m Error: mkdir build failed~! \033[0m"
fi

cd build
if [ "$?" -eq "0" ];then
	echo "\033[33m cd build  ok~! \033[0m"
else
	echo "\033[31m Error: cd build failed~! \033[0m"
fi

sudo cmake ./..
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cmake ./..  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake ./.. failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd .. #out build
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

cd .. #out curlpp-master
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi


echo "\033[33mstart to install the googletest-master\033[0m"
unzip googletest-master.zip
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file googletest-master.zip ok~! \033[0m"
else
	echo "\033[31m Error:unPack file googletest-master.zip failed~! \033[0m"
fi

cd googletest-master
if [ "$?" -eq "0" ];then
	echo "\033[33m cd googletest-master  ok~! \033[0m"
else
	echo "\033[31m Error: cd googletest-master failed~! \033[0m"
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo "\033[33m mkdir build  ok~! \033[0m"
else
	echo "\033[31m Error: mkdir build failed~! \033[0m"
fi

cd build
if [ "$?" -eq "0" ];then
	echo "\033[33m mkdir build  ok~! \033[0m"
else
	echo "\033[31m Error: mkdir build failed~! \033[0m"
fi

sudo cmake ..
if [ "$?" -eq "0" ];then
	echo "\033[33m  sudo cmake .. ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake .. failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd .. #out build
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

cd .. #out googletest-master
if [ "$?" -eq "0" ];then
	echo "\033[33m   ok~! \033[0m"
else
	echo "\033[31m Error:  failed~! \033[0m"
fi


echo "\033[33mstart to install the protobuf-cpp-3.4.1\033[0m"
#EDD 将/home/jjuv/mapgroup/for-apollo/protobuf-3.4.1/src/google/protobuf/stubs/strutil.h map_util.h  stringprintf.h 拷贝到local/include/google/proto。。/stubs
unzip protobuf-cpp-3.4.1.zip
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file protobuf-cpp-3.4.1.zip ok~! \033[0m"
else
	echo "\033[31m Error:unPack file protobuf-cpp-3.4.1.zip failed~! \033[0m"
fi

cd protobuf-3.4.1
if [ "$?" -eq "0" ];then
	echo "\033[33m cd protobuf-3.4.1  ok~! \033[0m"
else
	echo "\033[31m Error: cd protobuf-3.4.1 failed~! \033[0m"
fi

./autogen.sh
if [ "$?" -eq "0" ];then
	echo "\033[33m ./autogen.sh  ok~! \033[0m"
else
	echo "\033[31m Error: ./autogen.sh failed~! \033[0m"
fi

sudo ./configure
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo ./configure  ok~! \033[0m"
else
	echo "\033[31m Error: sudo ./configure failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

make check
if [ "$?" -eq "0" ];then
	echo "\033[33m   ok~! \033[0m"
else
	echo "\033[31m Error:  failed~! \033[0m"
fi

sudo make install 
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install   ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install  failed~! \033[0m"
fi

sudo ldconfig # refresh shared library cache 
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo ldconfig   ok~! \033[0m"
else
	echo "\033[31m Error: sudo ldconfig  failed~! \033[0m"
fi

sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp src/google/protobuf/stubs/strutil.h  /usr/local/include/google/protobuf/stubs  failed~! \033[0m"
fi

sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp src/google/protobuf/stubs/map_util.h  /usr/local/include/google/protobuf/stubs failed~! \033[0m"
fi

sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp src/google/protobuf/stubs/stringprintf.h  /usr/local/include/google/protobuf/stubs failed~! \033[0m"
fi

cd .. #out protobuf-cpp-3.4.1
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

echo "\033[33mstart to install the fileqpOASES-3.2.1\033[0m"
unzip qpOASES-3.2.1.zip
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file qpOASES-3.2.1.zip ok~! \033[0m"
else
	echo "\033[31m Error:unPack file qpOASES-3.2.1.zip failed~! \033[0m"
fi

cd qpOASES-3.2.1
if [ "$?" -eq "0" ];then
	echo "\033[33m cd qpOASES-3.2.1  ok~! \033[0m"
else
	echo "\033[31m Error: cd qpOASES-3.2.1  failed~! \033[0m"
fi
sudo cmake ./
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake ./ failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd .. #out protobuf-cpp-3.4.1
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

echo "\033[33mstart to install the opencv-3.2.0\033[0m"
tar -zxvf opencv-3.2.0.tar.gz	
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file opencv-3.2.0.tar.gz ok~! \033[0m"
else
	echo "\033[31m Error:unPack opencv-3.2.0.tar.gz file failed~! \033[0m"
fi

cd opencv-3.2.0
if [ "$?" -eq "0" ];then
	echo "\033[33m cd opencv-3.2.0  ok~! \033[0m"
else
	echo "\033[31m Error: cd opencv-3.2.0 failed~! \033[0m"
fi
sudo cmake ./
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cmake ./  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake ./  failed~! \033[0m"
fi

sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make failed~! \033[0m"
fi
sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

cd .. #out opencv-3.2.0
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

echo "\033[33mstart to install the Ipopt-3.12.8.tgz\033[0m"
tar -zxvf Ipopt-3.12.8.tgz	
if [ "$?" -eq "0" ];then
	echo "\033[33m unpack file Ipopt-3.12.8.tgz ok~! \033[0m"
else
	echo "\033[31m Error:unPack file failed~! \033[0m"
fi

cd Ipopt-3.12.8
if [ "$?" -eq "0" ];then
	echo "\033[33m cd Ipopt-3.12.8  ok~! \033[0m"
else
	echo "\033[31m Error: cd Ipopt-3.12.8 failed~! \033[0m"
fi

./configure
if [ "$?" -eq "0" ];then
	echo "\033[33m ./configure  ok~! \033[0m"
else
	echo "\033[31m Error: ./configure failed~! \033[0m"
fi
sudo make
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make  ok~! \033[0m"
else
	echo "\033[31m Error:  failed~! \033[0m"
fi

sudo make install
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make install  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make install failed~! \033[0m"
fi

sudo cp -r lib/. /usr/local/lib
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp -r lib/. /usr/local/lib  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp -r lib/. /usr/local/lib failed~! \033[0m"
fi

sudo cp -r include/. /usr/local/include
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp -r include/. /usr/local/include  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp -r include/. /usr/local/include failed~! \033[0m"
fi

cd .. #out Ipopt-3.12.8
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  
if [ "$?" -eq "0" ];then
	echo "\033[33m export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  ok~! \033[0m"
else
	echo "\033[31m Error:　export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH  failed~! \033[0m"
fi

sudo ldconfig  
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo ldconfig  ok~! \033[0m"
else
	echo "\033[31m Error: sudo ldconfig failed~! \033[0m"
fi

#install ccache.
sudo apt-get install  ccache
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo apt-get install  ccache  ok~! \033[0m"
else
	echo "\033[31m Error: sudo apt-get install  ccache failed~! \033[0m"
fi

#install caffe 
echo "\033[33mstart to install the caffe and copy to home! \033[0m"
cd caffe 
if [ "$?" -eq "0" ];then
	echo "\033[33m cd caffe   ok~! \033[0m"
else
	echo "\033[31m Error: cd caffe  failed~! \033[0m"
fi

mkdir build
if [ "$?" -eq "0" ];then
	echo "\033[33m mkdir build  ok~! \033[0m"
else
	echo "\033[31m Error: mkdir build failed~! \033[0m"
fi

cd build
if [ "$?" -eq "0" ];then
	echo "\033[33m cd build  ok~! \033[0m"
else
	echo "\033[31m Error: cd build failed~! \033[0m"
fi

sudo cmake ../.
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cmake ../.  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cmake ../. failed~! \033[0m"
fi

sudo make all
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo make all  ok~! \033[0m"
else
	echo "\033[31m Error: sudo make all failed~! \033[0m"
fi

cd ..
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi

cd ..
if [ "$?" -eq "0" ];then
	echo "\033[33m cd ..  ok~! \033[0m"
else
	echo "\033[31m Error: cd .. failed~! \033[0m"
fi


sudo cp -r caffe ~/.
if [ "$?" -eq "0" ];then
	echo "\033[33m sudo cp -r caffe ~/.  ok~! \033[0m"
else
	echo "\033[31m Error: sudo cp -r caffe ~/. failed~! \033[0m"
fi

