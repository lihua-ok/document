﻿
# LQR Control (纵向控制)

标签： 文献阅读
作者：潘世举
2018.04.16


这是加州理工大学控制与动力系统的课程，时间：2006.01.11。看得不是很懂，前半部分能看懂，在　　带有特征多项式的闭环系统　　部分，就看不懂了。

---

摘要：提供了LQR的简单推导和对描述了基于LQR的补偿器的设计、积分反馈对消除稳态误差的作用、汽车速度控制。

---

##线性二次型调节器

有限时域内：
$$\dot{x}=Ax+Bu　　　x\in R^n,u\in R^n,x_0　given$$
$$\widetilde{J}=\frac{1}{2}\int^{T}_{0}(x^TQx+u^TRu)dt+\frac{1}{2}x^T(T)Px(T)$$
其中，$Q\geq0,R\geq0,P_1\geq0$是对称正定（半正定）矩阵，忽略$\frac{1}{2}$以简化推导。
最大值求解原理：
$$H=x^TQx+u^TRu+\color{red}{\lambda^T}(Ax+Bu)$$
$$\dot{x}=Ax+Bu,　　x(0)=x_0$$
$$\dot{\lambda}=Qx+A^T\lambda,　　\lambda=Px$$
$$得：u=-R^{-1}B^T\lambda$$
得到了最优解，适用于求解两点边值问题。猜想解的形式是$\lambda(t)=P(t)x(t)$，那么
$$\dot{\lambda}=\dot{P}x+P\dot{x}=\dot{P}x+P(Ax-BR^{-1}B^TPx)$$
$$-\dot{P}x-PAx+PBR^{-1}BPx=Qx+A^TPx$$
若$P$满足下式，则方程式满足条件：
$$-\dot{P}=PA+A^TP-PBR^{-1}B^TP+Q$$
注意：
1.上式被称为Riccati微分方程；
2.在时间上求逆$u(t)=-R^{-1}B^TP(t)x$，得到时变的反馈控制——从任一状态到起始状态；
3.令$T=\infty$，可消除终端约束：
$J=\int_{0}^{\infty}(x^TQx+u^TRu)dt$
$u=-\underbrace{R^{-1}B^TP}_{\rm K}x,P是连续的$
$0=PA+A^TP-PBR^{-1}B^TP+Q$
上式被称为Riccati代数方程。
4.需要$R>0,Q\geq0$。

---

##参考轨迹状态反馈
已知系统：$\dot{x}=f(x,u)$，可行参考：$(x_d,u_d)$，设计补偿器$u=\alpha(x,x_d,u_d)$使$\lim_{t\to\infty} x-x_d=0$，即轨迹跟踪问题。
首先构造误差系统，假设：$f(x,u)=f(x)+g(x)u$（该系统状态非线性，但输入线性。在应用中经常用到）。令$e=x-x_d,v=u-u_d$,
则：
$$\dot{e}=\dot{x}-\dot{x_d}=f(x)+g(x)u-f(x_d)-g(x_d)$$
　　　　　　　　　　　　　　　　　$=f(e+x_d)-f(x_d)+g(e+x_d)(v+u_d)-g(x_d)u_d$
　　　　　　　　　　　　　　　　　$=F(e,v,x_d(t),u_d(t))$
　　　　　　　　　　　　　　　　　
通常，此系统是时变系统。
假设控制器的效果很好，$e$就很小，可在$e=0$附近进行线性化：
$$\dot{e}\approx A(t)e+B(t)v$$
其中，$A(t)=\frac{\partial F}{\partial e}|_{(x_d(t),u_d(t))}$，$B(t)=\frac{\partial F}{\partial v}|_{(x_d(t),u_d(t))}$。$A,B$一般只取决于$x_d$，因此$A(t)=A(x_d),B(t)=B(x_d)$。
假设$x_d,u_d$是常数或者缓慢变化（在性能指标内），那就只考虑$A(x_d),B(u_d)$给定的线性系统。如果为每一个$x_d$设计状态反馈控制器$K(x_d)$，那么可采用反馈调节系统：
$$v=K(x_d)e$$
则：$u=K(x_d)(x-x_d)+u_d$
这种形式的控制器被称为带有前馈$u_d$的增量式线性控制器。
在线性系统的特殊情况下，即误差动力学与系统动力学一致时，不需要根据$x_d$计算增量，很方便的计算出常数增益$K$：
$$u=K(x-x_d)+u_d$$

---

##积分作用
基本的方法是建立一个状态（计算误差信号的积分）作为反馈项，添加新状态$z$来描述系统：
$$\frac{d}{dt}\begin{bmatrix} x \\ z\\ \end{bmatrix}=\begin{bmatrix} Ax+Bu \\ y-r \\ \end{bmatrix}=\begin{bmatrix} Ax+Bu \\ Cx-r \end{bmatrix}$$
其中，$z$是期望输出$r$和实际输出$y$的误差的积分，如果找到某个补偿器使系统稳定，那么认为在稳定状态$\dot{z}=0$，即$y=r$。
在增强系统中，通常设计符合下面控制律的状态空间控制器：
$$u=-K(x-x_d)-K_iz+u_d$$
$K$是通常的状态反馈项，$K_i$是积分项，$u_d$为$\color{red}{标称模型???}$(nominal model)设置的参考输入。对于跃阶输入，系统平衡点如下式：
$$x_e=-(A-BK)^{-1}B(u_d-K_iz_e)$$
其中，$z_e$的值不确定，但是满足：$\dot{z}=y-r=0$，说明在平衡点的输出与参考输出相等。只要系统稳定，其值独立于$A,B,K$。
最终的补偿器是：
$$u=-K(x-x_e)-K_iz+u_d$$
$$\dot{z}=y-r$$
现在将积分器的动态作为控制器规范的一部分。

---

##实例：速度控制
在坡度、风阻、道路情况发生变化时，调节油门开度以保持恒定的速度。
![控制流程图](https://upload-images.jianshu.io/upload_images/11672955-e93aa289402f0165.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
$v$——实际车速；
$v_r$——目标车速；
$u$——控制信号（控制油门开度）；
$T$——发动机扭矩（正比于燃油喷射率，取决于发动机角速度$\omega$）；
最大扭矩$T_m$对应角速度$\omega_m$；
$n$齿轮传动比；
$r$车轮半径；
$F$——驱动力；
$\theta$——坡度；
$F_d$——干扰力；
$F_g$——坡道阻力；
$F_r$——滚动阻力；
$C_r$——滚动摩擦系数；
$F_a$——空气阻力；
$C_v$——空气阻力系数；
$A$——迎风面积；

$$m\dot{v}=F-F_d　　　(1)$$
$$\omega=\frac{n}{r}v=\alpha_n v$$
$$F=u\frac{n}{r}T(\omega)=u\alpha _nT(\alpha_n v)$$
扭矩曲线简单表示为：
$$T(\omega)=uT_m(1-\beta(\frac{\omega}{\omega_m}-1)^2)　　(2)$$
![扭矩曲线](https://upload-images.jianshu.io/upload_images/11672955-d221153fbaa0e63b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
$$F_g=mg\sin\theta$$
$$F_r=mgC_r$$
$$F_a=\frac{1}{2}\rho C_vAv^2$$

车辆可被模型化为：
$$m\dot{v}=F-F_d$$
$$F=\alpha _nuT(\alpha_n v) 　　　　　(3) $$   
$$F_d=mgC_r+\frac{1}{2}\rho C_vAv^2+mg\theta$$

模型(3)是一阶动力系统，$v$既是状态又是输出，$u$是输入，$F_d$是干扰。由于转矩曲线和空气阻力的非线性特征，系统也是非线性。
###线性化
假设，稳定在车速$v_0=25$，道路水平$\theta=0$，油门位置已知$u_0=0.218$：

$$u_0\alpha_nT(\alpha_nv_0)-mgC_r-\frac{1}{2}\rho C_vAv_0^2-mg\theta_0=0　　(4)$$
在平衡点对式(3)泰勒展开，忽略二阶及高阶项：
$$m\frac{d(v-v_0)}{dt}=ma(v-v_0)-mg(\theta-\theta_0)+mb(u-u_0)　　(5)$$
其中$ma=u_0\alpha_n^2{T}^\prime(\alpha_nv_0)-\rho C_vAv_0$，$mb=\alpha_nT(\alpha_nv_0)$　　　　　　　$(6)$
令$\rho=1.3，C_v=0.32，A=2.4，C_r=0.01$
$$\frac{d(v-v_0)}{dt}=-0.004(v-v_0)+3.61(u-u_0)-9.81\theta　　(7)$$
在平衡点$v_0,u_0$处的线性化动力学：
$$\dot{\tilde{v}}=a\tilde{v}-mg\theta+mb\tilde{u}$$
$$y=v=\tilde{v}+v_0$$

其中$\tilde{v}=v-v_0,\tilde{u}=u-u_0$。

###控制设计
$$\dot{\tilde{v}}=\frac{a}{m}\tilde{v}-g\theta+b\tilde{u}$$
$$\dot{z}=r-y=(r-v_0)-\tilde{v}$$
状态空间形式：
$$\frac{d}{dt} \begin{bmatrix} \tilde{x} \\ z\\ \end{bmatrix} =\begin{bmatrix}\frac{a}{m} & 0 \\ -1 & 0 \\ \end{bmatrix} \begin{bmatrix} \tilde{v} \\ z \end{bmatrix} + \begin{bmatrix} b\ \\ 0\\ \end{bmatrix}u + \begin{bmatrix} -g \\ 0 \\ \end{bmatrix} \theta + \begin{bmatrix} 0 \\ r-v_0 \\ \end{bmatrix}$$
平衡点时，$\tilde{z}=0$，即$v=v_0+\tilde{v}$，与期望的参考速度$r$相等。
控制器有如下形式：
$$\dot{z}=r-y$$
$$u=-K\tilde{v}-K_iz+u_d$$
其中$K,K_i,u_d$满足：使系统稳定并为参考速度提供实时输入。

一般希望设计具有特征多项式的闭环系统：
$$\lambda(s)=s^2+a_1s+a_2$$
在无坡$\theta=0$情况下：
$$det(sI-(A-BK))=s^2+(bK-a/m)s-bK_z$$
令$K=(a_1+\frac{a}{m})/b, K_z=-a_2/b$。




---






















---




