﻿# 粒子相对于旋转框架的运动(apollo　MPC控制)

标签（空格分隔）： 文献阅读
作者：潘世举
2018.04.25
$\color{red}{本文献是Apollo中MPC控制的参考文献}$。

描述了固定刚体的坐标加速度与惯性坐标加速度之间的关系。该公式可用于获得具有偏航旅、滚动和俯仰旋转运动的车辆惯性加速度值。文中，该公式用于获得沿旋转轴线偏航运动的车辆横向加速度。

---
##粒子相对于旋转框架的运动关于道路误差的动力学模型
惯性坐标系$XYZ$，刚体坐标系$xyz$。假设两个坐标系具有　相同的去向，假设刚体的角速度为 $\vec{\Omega} $ 。
![惯性坐标系和刚体坐标系](https://upload-images.jianshu.io/upload_images/11672955-b446981bcc9bfd59.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

假设粒子$P$，在惯性坐标系中$[X　Y　Z]^T$，刚体坐标系中$[x　y　z]^T$；
从惯性坐标系原点到$P$的矢量$\vec{r}$；
惯性坐标系下的粒子加速度与刚体坐标系中的加速度有关：
$$\frac{d^2}{dt^2}\left(\begin{matrix} X \\ Y \\ Z \\ \end{matrix}\right)=\frac{d^2}{dt^2}\left( \begin{matrix} x \\ y \\ z \\ \end{matrix} \right)+ \vec{\Omega} \times (\vec{\Omega}\times \vec{r})+\vec{\dot{\Omega}}\times\vec{r}+2\vec{\Omega}\times\vec{\dot{r}}　　(2.33)$$
方程右侧的所有矢量都用刚体坐标系表示，将$2.33$应用于下图系统
![旋转坐标系下的横向系统](https://upload-images.jianshu.io/upload_images/11672955-0541460a49ef3e72.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
$x,y,z$轴的单位向量是$\hat{i},\hat{j},\hat{k}$；

$$\vec{\Omega}=\dot{\psi}\hat{k}　　(2.34)$$

$$\vec{r}=-R\hat{j}　　(2.35)$$

由$2.33$得，惯性坐标系下的加速度与刚体坐标系下的关系：
$$\vec{a}_{inertial}=\vec{\Omega} \times (\vec{\Omega}\times \vec{r})+\vec{\dot{\Omega}}\times\vec{r}+2\vec{\Omega}\times\vec{\dot{r}}+\vec{a}_{body_-fixed}$$

或者：

$$\vec{a}_{inertial}=\dot{\psi}\hat{k} \times (\dot{\psi}\hat{k} \times (-R\hat{j})) + \ddot{\psi}\hat{k}\times(-R\hat{j})+2\dot{\psi}\hat{k} \times(-R\hat{j})+\ddot{x}\hat{i}+\ddot{y}\hat{j}+$$

或者：

$$\vec{a}_{inertial}=\dot{\psi}^2R\hat{j}+(R\ddot{\psi}+2\dot{\psi}\dot{R})\hat{i}+\ddot{x}\hat{i}+\ddot{y}\hat{j}　　(2.36)$$
 
因此$a_y=\dot{\psi}^2R+\ddot{y}=V_x\dot{\psi}+\ddot{y}$

因此，沿着y轴的惯性加速度是：
$$a_y=\ddot{y}+V_x\dot{\psi}　　(2.37)$$

---
##关于道路误差的动力学模型
与文献<基于MPC的重型车辆编队横向控制>，相似：

车辆的重心从车道中心线的距离$e_1$；
车辆相对于道路的位置误差$e_2$；
在恒定半径$R$的道路上保持恒定纵向速度$V_x$的车辆,同样，假设半径$R$很大，因此可以得到与前一节相同的小角度假设。
车辆期望的方向变化率：
$$\dot{\psi}_{des}=\frac{V_x}{R}　　(2.38)$$
车辆期望的加速度：
$$\frac{V_x^2}{R}=V_x\dot{\psi}_{des}　　(2.39)$$

定义：

$$\ddot{e}_1=(\ddot{y}+V_x\dot{\psi})-\frac{V_x^2}{R}=\ddot{y}+V_x(\dot{\psi}-\dot{\psi}_{des})　　(2.40)$$

$$e_2=\psi-\psi_{des}　　(2.41)$$

$$\dot{e}_1=\dot{y}+V_x(\psi-\psi_{des})　　(2.42)$$

此处的$\psi-\psi_{des}$，相当于$\sin(\psi-\psi_{des})$，只是由于半径很大，航偏角较小，近似处理。$2.42$在$V_x$恒定时，$2.42$由$2.40$对时间求导得到；如果速度不是常数，则：

$$\dot{e}_1=\dot{y}+\int V_xe_2dt$$

这将产生一个非线性和时变的模型，对于控制系统设计是没有用的。因此，所采取的方法是假定纵向速度是恒定的，并获得LTI(linear time-invarian 线性时不变)模型。

将$2.41,2.42$代入$2.21,2.22$得：

$$m(\ddot{y}+\dot{\psi}V_x)　　(2.21)$$

$$I_z\ddot{\psi}=l_fF_{yf}-l_r-F_{yr}　　(2.22)$$

$$F_{yr}=C_r\frac{-\dot{y}+l_r\dot{\psi}}{V_x}$$
$$F_{yf}=C_f(\delta-\frac{\dot{y}+l_f\dot{\psi}}{V_x})$$

将$\ddot{y}、\dot{y}、\dot{\psi}$代入，$F_{yf}、F_{yr}$的推导过程可参考文献$\color{red}{基于MPC的重型车辆编队的横向控制}$。得到模型($\color{red}{注意：}$假定纵向速度为恒定)：
$$\begin{bmatrix} \dot{e}_1 \\\ddot{e}_1 \\ \dot{e}_2 \\ \ddot{e}_2 \end{bmatrix} = 
\begin{bmatrix} 0 & 1 & 0 & 0 \\ 0 & -\frac{C_f+C_r}{mV_x} & \frac{C_f+C_r}{m} & \frac{-C_fl_f+C_rl_r}{mV_x} \\ 
0 & 0 & 0 & 1 \\ 0 & -\frac{C_fl_f-C_rl_r}{I_zV_x} &  -\frac{C_fl_f-C_rl_r}{I_z} &  -\frac{C_fl_f^2+C_rl_r^2}{I_zV_x}\end{bmatrix}  \begin{bmatrix} e_1 \\ \dot{e}_1  \\ e_2 \\ \dot{e}_2 \\\end{bmatrix}$$
$$+\begin{bmatrix} 0 \\ \frac{C_f}{m} \\ 0 \\\frac{C_fl_f}{I_z}  \\ \end{bmatrix}\delta + \begin{bmatrix} 0 \\ -\frac{C_fl_f-C_rl_r}{mV_x}-V_x \\ 0 \\ -\frac{C_fl_f^2 + C_rl_r^2}{I_zV_x} \end{bmatrix} \dot{\psi}_{des}　　(2.45)$$
