﻿# 基于MPC的重型车辆编队横向控制
标签（空格分隔）： 文献阅读
作者：潘世举
2018.04.20

---

##1.理论背景

概述了使用的理论（MPC方法）、操作标准公式从而使控制更平滑、添加松弛变量、在预测模型中使用前馈、线性卡尔曼滤波理论。

---
###1.1模型预测控制
模型预测控制器的核心是一种在线算法，使用一个模型预测系统的未来行为，并以最小化约束的二次成本函数的形式，返回基于数值优化的控制信号。
####预测
使用线性MPC、离散状态空间表示预测模型：
$$z(k+1)=A_dz(k)+B_du(k)　　(2.1)$$
$u(k)、z(k)$是在采样时刻$k$时的控制输入和状态，$A_d$是离散系统矩阵，$B_d$离散输入矩阵。系统的行为被预测到未来的$N$步，称为预测层。使用一个称为控制层的预测输入序列如下：
$$z(k+1+i|k)=A_dz(k_i|k)+B_du(k+i|k)　i=0,1,\cdots,N-1　　(2.2)$$
控制层通常与预测层相同，但可以缩短，以减少计算复杂度。
####优化
令成本函数$J$最小，得到预测状态和控制输入，本文使用的是二次成本函数：
$$J(k)=\sum^{N_h-1}_{i=0}||z(k+1+i)-z_{ref}(k+1+i)||^2_Q+\sum^{N_u-1}_{i=0}||u(k+i)-u_{ref}(k+i)||^2_R　　(2.3)$$
其中$N_h,N_u$分别是预测层和控制层，$Q,R$是在状态和控制输入上包含标量权重的正定矩阵，$z_ref$是应跟随的未来状态参考，$u_ref$是未来参考输入，一般设为$0$。在时刻$k$，使二次成本函数最小的控制序列$u^{\ast}(k)$
$$u^{\ast}(k)=minJ(k)　　(2.4)$$
在产生最优化问题时，成本函数受系统约束(2.1),也可在状态和控制输入上进一步约束：
$$minJ(k)=min\sum^{N_h-1}_{i=0}||z(k+1+i)-z_{ref}(k+1+i)||^2_Q+\sum^{N_u-1}_{i=0}||u(k+i)-u_{ref}(k+i)||^2_R　　(2.5)$$
$$subject to: x(0)=x_0$$
$$\forall i\in[0,N_h]:　z(k+1+i)=Az(k+i)+Bu(k+i)$$
$$\forall i\in[0,N_h]:　z(k+i)\in Z$$
$$\forall i\in[0,N_h]:　u(k+i)\in U$$
其中$Z,U$分别是施加于状态和控制信号的附加约束。
####滚动框架
在线计算过程中，在每一采样时刻$k$使$(2.5)$最小化的最优控制输入序列$u^{\ast}(k)$只有第一个控制输入向量会被应用。在每次最小化计算是，预测长度不变，从而形成后退或者移动的$\color{red}{horizon}$。
$$\begin{array}{c|lcr} 
 & MPC算法过程 \\
 \hline
 1. & 测量x(k|k) \\
 2. & 通过(2.5)计算出控制信号u \\
 3. & 将第一控制信号应用到实际　\\
 4. & 等到下一时刻(k+1) \\
 5. & 重复步骤1 \\
\end{array}$$

---

###1.2平滑控制
控制信号变化$\Delta U(k)=u(k)-u(k-1)$,如果太大的话，与车辆的实际和舒适度向违背，存在潜在的危险，因此要使之最小化。本章介绍了如何限制$u(k),\Delta u(k)$的变化。
####限制$u(k)$的变化
可以改变预测模型，使用控制增量$\Delta u(k)$代替$u(k)$作为输入，这样的话就直接限制了增量的变化，从而限制了$u(k)$的变化量。
$$z(k+1)=A_dz(k)+B_du(k)　　(2.6)$$
$$\begin{bmatrix} z(k+1) \\ u(k) \end{bmatrix}=\underbrace{\begin{bmatrix} A_d & B_d \\ 0 & 1 \\ \end{bmatrix}}_{\rm\hat{A} } \underbrace{\begin{bmatrix} z(k)\\ u(k-1) \end{bmatrix}}_{\rm \hat{z}(k)}+\underbrace{\begin{bmatrix} B_d \\ 1 \\\end{bmatrix}}_{\rm \hat{B}} \Delta u(k)　　(2.7)$$
####限制$\Delta u(k)$的变化
限制两个连续的$\Delta u$，即：$\Delta ^2u(k)=\Delta u(k)-\Delta u(k-1)$
$$u(k)=\Delta u(k)+u(k-1)$$
$$\Delta u(k)=\Delta ^2 u(k)+\Delta u(k-1)$$
得：
$$u(k)=u(k-1)+\Delta u(k-1)+\Delta ^2 u(k)　　(2.8)$$

这样，线性系统可表示为：
$$\xi^+=A\xi+B\Delta ^2u　　(2.10)$$
其中，$\xi=\begin{bmatrix} z(k+1) \\ u(k) \\ \Delta u(k) \\ \end{bmatrix}$，$A=\begin{bmatrix} A_d & B_d & B_d \\ 0 & 1 & 1 \\ 0 & 0 & 1 \\ \end{bmatrix}$，$B=\begin{bmatrix} B_d \\ 1 \\ 1 \\ \end{bmatrix}$

---
###1.3约束
由于预测模型与实际的差别，导致硬约束出问题，MPC计算出的结果在约束内，但是实际车辆会稍微超出约束，在约束外得不到可行的优化。处理这个问题的方法之一是通过增加非负松弛变量$\varepsilon_{1:N}$来增加约束的大小，与权矩阵$S$一起加入成本函数&J&。未来使松弛变量保持在$0$，权矩阵$S$比$Q,R$大得多。约束的作用是使状态在预测区间内保持在最大值与最小值之间：
$$z_{1_{min}}\leq z_1(k)\leq z_{1_{max}}　　(2.11)$$
加入松弛变量后：
$$z_{1_{min}}-\varepsilon(k)\leq z_1(k)\leq z_{1_{max}}+\varepsilon(k), k=1,\cdots,N_h-1.　　(2.12)$$
这样，成本函数如下：
$$J(k)=\sum^{N_h-1}_{i=0}||z(k+1+i)-z_{ref}(k+1+i)||^2_Q+$$
$$\sum^{N_u-1}_{i=0}||u(k+i)-u_{ref}(k+i)||^2_R+$$
$$||\varepsilon(k+1+i)||^2_S　(2.13)$$

---
###1.4利用预览信息
作用才状态上的扰动可用下式表示：
$$\xi^+=A\xi_{k+N_h-1}+B\Delta ^2u_{k+N_h-1}+W\kappa_{k+N_h-1}　　(2.14)$$
使用的的预览信息通常称为前馈。

---
###1.5卡尔曼滤波器
卡尔曼滤波器是一种基于测量的动态线性系统状态估计滤波器。其思想是考虑一个过程模型，它是一组描述要估计的状态的数学方程，并将这个过程模型与测量的变化相结合，以获得更精确的读数。
$$z_k=Az_(k-1)+Bu_k+\omega_k　　(2.15)$$
$ω$假设为矩阵$Q$的零均值白噪声协方差，即：$W~N(0,Q)$真实状态的测量是通过：
$$y_k=Cz_k+v_k　　(2.16)$$
其中，$C$是测量矩阵，将真实状态$z_k$映射到测量观测$y_k$上。
$v_k$是协方差矩阵$R$的零均值白噪声的测量噪声。递归滤波器的迭代有两个步骤，即也测更新步骤和测量更新步骤。每一次迭代的输出是估计状态的均值$\hat{z}_k$，以及它们的协方差矩阵$P_k$

$$\begin{array}{lcr} 
 卡尔曼滤波过程 \\
\hline
  基于z_0的状态估计初始预测：z_k=Az_0+Bu_0 \\
  基于P_0的协方差估计初始预测：P_k=P_0AP_0+Q_k \\
  while　measuring　do　\\
  　　计算测量误差：\tilde{y}=y_k-C\hat{z}_k \\
  　　计算最有卡尔曼增益：K_k=P_kC(CPC^T+R_k)^{-1} \\
  　　基于测量的状态估计修正：\hat{x}_k=\hat{z}_k+K_k\tilde{y}_k \\
  　　基于测量的协方差估计修正：P_k=(I-K_kC)P_k \\
  　　状态估计预测：\hat{z}_k=Az_{k-1}+Bu_{k-1} \\
  　　协方差估计预测：P_k=P_{k-1}AP_{k-1}+Q_k \\
  　　return　\hat{z}_k\\
  　　return　P_k\\
  　　k\gets k+1\\
  end　while\\
\end{array}$$

---
##2.系统描述

**直接跳到模型化部分**
###2.1车辆横向动力学
自行车模型，值考虑横向和偏航运动，没有滚动。局部横向速度$\dot{y}$和角速度$\dot{\Psi}$
![Bicycle model](https://upload-images.jianshu.io/upload_images/11672955-94b3166906742568.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
前后轮侧向力$F_{ry},F_{fy}$；
滑动角$\alpha_r,\alpha_f$；
转向轮转角$\delta_f$；
轮胎前后速度$v_f,v_r$；
局部纵向和横向速度$\dot{x},\dot{y}$；
全局坐标系下的航向角$\Psi$；
前后轴到重心的距离$l_f,l_r$；
总质量$m$；
总的横向加速度$a_y$；
前后轮的侧偏刚度$C_f,C_r$；
前后轮侧滑角$\beta_f,\beta_r$；
车辆$z$轴的转动惯量$I_z$；
$$ma_y=F_{ry}+F_{fy}　　(3.2)$$
$$a_y=\ddot{y} + \dot{x} \dot{\Psi}　　(3.3)$$
$$m(\ddot{y}+\dot{x}\dot{\Psi}）=F_{ry}+F_{fy}　　(3.4)$$
实验证明，在小角度下，横向力$F_{rf},F_{fy}$正比于轮胎大小：
$$F_{ry}=2C_r\alpha_r　　(3.5)$$



$$F_{fy}=2C_f\alpha_f　　(3.6)$$



$$\alpha_r=-\beta_r　　(3.7)$$



$$\alpha_f=\delta_f-\beta_f　　(3.8)$$



$$\tan(\beta_r)\approx \beta_r =\frac{\dot{y}-l_r\dot{\Psi}}{\dot{x}}　　(3.9) $$
$$\tan(\beta_f)\approx\beta_f = \frac{\dot{y}+l_f\dot{\Psi}}{\dot{x}}　　(3.10)$$

由$3.5--.3.10$推导出侧向力：
$$F_{ry}=2C_r(\frac{-\dot{y}+l_r\dot{\Psi}}{\dot{x}})　　(3.11)$$


$$F_{fy}=2C_f(\delta-\frac{\dot{y}+l_f\dot{\Psi}}{\dot{x}})　　(3.12)$$
将$3.11、3.12$代入$3.4$，计算出横向加速度：$\color{red}{此处是自行车模型，因此计算侧向力是没有乘以２}$
$$\ddot{y}=-\frac{C_f+C_r}{m\dot{x}}\dot{y}-\frac{C_fl_f-C_rl_r}{m\dot{x}} \dot{ \Psi }-\dot{x}\dot{\Psi}+\frac{C_f}{m}\delta_f　　(3.13)$$

对于角加速度$\ddot{\Psi}$，根据车辆$z$轴的力矩平衡：
$$I_z\ddot{\Psi}=l_fF_{fy}-l_rF_{ry}　　(3.14)$$

角加速度为：
$$\ddot{\Psi}=-\frac{C_fl_f-C_rl_r}{I_z\dot{x}}\dot{y}-\frac{C_fl_f^2+C_rl_r^2}{I_z\dot{x}}\dot{\Psi}+\frac{C_fl_f}{I_z}\delta_f　　(3.15)$$

状态方程为：

$$\begin{bmatrix} \ddot{y} \\ \ddot{\Psi} \end{bmatrix}=\begin{bmatrix} -\frac{C_f+C_r}{m\dot{x}} & -\frac{C_fl_f-C_rl_r}{m\dot{x}}-\dot{x} \\
-\frac{C_fl_f-C_rl_r}{I_z\dot{x}} & -\frac{C_fl_f^2+C_rl_r^2}{I_z\dot{x}} \\
\end{bmatrix}\begin{bmatrix} \dot{y} \\ \dot{\Psi} \end{bmatrix} + \begin{bmatrix} \frac{C_f}{m} \\ \frac{C_fl_f}{I_z} \end{bmatrix}\delta_f　　(3.16)$$

###2.2车辆位置动力学
在道路坐标框架下：
车辆与参考点的航向角偏差$\Psi_{lc}$；
车前相机到参考点的距离$y_{lc}$；



![新加入两个状态](https://upload-images.jianshu.io/upload_images/11672955-7f55b4e8d82b0b47.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**下面的非线性方程描述了横向位置动力学**
相对于全局惯性系，车和路的航向角$\Psi,\Psi_r$；
参考点角速度$\dot{\Psi}_r=\kappa\dot{s}$；
参考点曲率$\kappa$；
参考点速度$\dot{s}$；
在车辆坐标系中的速度$\dot{x},\dot{y},\dot{y}_s$；
前轴到相机传感器的距离$l_s$；

$$\dot{\Psi}_{lc}=\dot{\Psi}-\dot{\Psi_r}=\dot{\Psi}-\kappa\dot{s}　　(3.17)$$


$$\dot{y}_{lc}=\dot{y}_s\cos(\Psi_{lc})+\dot{x}\Psi_{lc}　　\color{red}{(3.18)}$$


$$\dot{s}=\dot{x}\cos(\Psi_{lc})+\dot{y}_s\sin(\Psi_{lc})　　(3.19)$$


$$\dot{y}_s=\dot{y}+(l_f+l_s)\dot{\Psi}　　(3.20)$$


现代公路中，$半径>500m$的情况下，$\Psi_{lc}$很小，通过这种近似，可将非线性方程简化成如下的线性方程：

$$\dot{s}=\dot{x}$$

$$\dot{y}_{lc}=\dot{y}+(l_f+l_s)\dot{\Psi}+\dot{x}\Psi_{lc}$$

$$\dot{\Psi}_{lc}=\dot{\Psi}-\dot{\Psi}_r=\dot{\Psi}-\kappa \dot{s}=\dot{\Psi}-\dot{\Psi}_r=\dot{\Psi}-\kappa \dot{x}$$

**扩展状态空间方程**

$$\begin{bmatrix} \ddot{y} \\ \ddot{\Psi} \\ \dot{\Psi}_{cl} \\ \dot{y}_{cl} \\ \end{bmatrix}=\begin{bmatrix} -\frac{C_f+C_r}{m\dot{x}} & -\frac{C_fl_f-C_rl_r}{m\dot{x}}-\dot{x} & 0 & 0  \\
-\frac{C_fl_f-C_rl_r}{I_z\dot{x}} & -\frac{C_fl_f^2+C_rl_r^2}{I_z\dot{x}} & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 1 & l_f+l_s & \dot{x} & 0 \\
\end{bmatrix} \begin{bmatrix} \dot{y} \\ \dot{\Psi} \\ \Psi_{lc} \\ y_{lc} \end{bmatrix} + \begin{bmatrix} \frac{C_f}{m} \\ \frac{C_fl_f}{I_z} \\ 0 \\ 0 \\ \end{bmatrix}\delta_f
+\begin{bmatrix} 0 \\ 0 \\ \dot{x} \\ 0 \\ \end{bmatrix} \kappa　　(3.23)$$


###2.3离散化

为了找到线性连续系统的解析解，首先对表达式
$$\dot{x}(t)=A_cx(t)+B_cu(t)　　(3.24)$$
进行重新排列，然后通过引入积分因子，得到下列方程：
$$e^{-A_ct}\dot{x}(t)-e^{-A_ct}Ax(t)=e^{-A_ct}B_cu(t)　　(3.25)$$

$$\frac{d}{dt}(e^{-A_ct}x(t))=e^{-A_ct}B_cu(t)　　\color{red}{(3.26)}$$

$$\int^{t}_{t_0}\frac{d}{dt}(e^{-A_ct}x(t))d\tau=\int^{t}_{t_0}e^{-A_c\tau}B_cu(\tau)d\tau　　(3.27)$$

$$e^{-A_ct}x(t)-e^{-A_ct_0}x(t_0)=\int^{t}_{t_0}e^{-A_c\tau}B_cu(\tau)d\tau　　(3.28)$$

$$e^{-A_ct}x(t)=e^{-A_ct_0}x(t_0)+\int^{t}_{t_0}e^{-A_c\tau}B_cu(\tau)d\tau　　(3.29)$$

$$x(t)=e^{A_c(t-t_0)}x(t_0)+\frac{1}{e^{-A_ct}}\int^{t}_{t_0}e^{-A_c\tau}B_cu(\tau)d\tau　　(3.30)$$

$$x(t)=e^{A_c(t-t_0)}x(t_0)+\int^{t}_{t_0}e^{A_c(t-\tau)}B_cu(\tau)d\tau　　(3.31)$$

以$T$为采样周期定义离散解：

$$x(t):=x(kT),k=0,1,2,\dots　　(3.32)$$

假设控制信号$u$在采样周期内保持恒定，即零阶保持（zero order hold,ZOH），令$t_0=kT,t=(k+1)T$，代入$3.31$得：

$$x((k+1)T)=e^{A_c(k+1)T-kT}x(kT)+\int^{(k+1)T}_{kT}e^{A_c((k+1)T-\tau)}B_cu(\tau)d\tau　　(3.33)$$

$$x_{k+1}=e^{A_cT}x_k+\int^{(k+1)T}_{kT}e^{A_c((k+1)T-\tau)}B_cd\tau \cdot  u(k)　　(3.34)$$
令$\gamma=(k+1)T-\tau$：

$$x_{k+1}=e^{A_cT}x_k+\int^{T}_{0}e^{A_c\gamma}B_cd\gamma \cdot u(k)　　(3.35)$$

与$3.24$对比，离散时间状态空间矩阵可以写为：
$$A_d=e^{A_cT}　　(3.36)$$
$$B_d=\int^{T}_{0}e^{A_c\gamma}B_cd\gamma　　(3.37)$$

上式可用无限和表示，但在实际应用上，一般用有限和（限制综合变量$i$）来近似：

$$A_d=e^{A_cT}\approx \sum^{i_{sufficient}}_{i=0}\frac{(A_cT)^i}{i!}　　(3.39)$$

$$B_d=\int^{T}_{0}e^{A_c\gamma}B_cd\gamma \approx \sum^{i_{sufficient}}_{i=0}\frac{(A_cT)^i}{(i+1)!}B_cT　　(3.40)$$

本文的$i_{sufficient}$一般在$5$左右。


###2.4离散模型
用$3.39,3.40$对连续模型$3.23$离散化，并用转向轮的加速度作为控制输入，得到全离散模型：
$$\begin{bmatrix} z(k+1) \\ u(k) \\ \Delta u(k) \\ \end{bmatrix} = 
\begin{bmatrix} A_d & B_d & B_d \\ 0 & 1 & 1 \\ 0 & 0 & 1 \\ \end{bmatrix} \begin{bmatrix} z(k) \\ u(k-1) \\ \Delta u(k-1) \end{bmatrix} +
\begin{bmatrix} B_d \\ 1 \\ 1 \\ \end{bmatrix}\Delta^2u(k)+\begin{bmatrix} W_d \\ 0 \\ 0 \\ \end{bmatrix} \kappa (k)　(3.41)$$
对应与$2.14$









































