﻿# EMplanner

Author: QiYao

[TOC]

---------
![apollo-EM.png](http://upload-images.jianshu.io/upload_images/11020056-f1e2fa9d73450bd5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
由tasks组成。

### dp_poly_path
![DP-path.png](http://upload-images.jianshu.io/upload_images/11020056-5aadf6cc0c3d3dda.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
使用动态规划进行的路径规划，通过在路径中按照固定间隔进行撒点，而后对这些点进行cost计算，计算后选择最小的cost。
函数入口是继承父类PathOptimizer
```C++
Status DpPolyPathOptimizer::Process(const SpeedData &speed_data,
                                    const ReferenceLine &,
                                    const common::TrajectoryPoint &init_point,
                                    PathData *const path_data)
```
#### 算法流程
```flow
st=>start: dp_road_graph.FindPathTunnel(init_point,reference_line_info_->path_decision()->path_obstacles().Items(),path_data)
op=>start: XYToSL:将初始点转化为SL坐标系
frenet=>start: CalculateFrenetPoint:将SL坐标系加上一阶 二阶导数，变成frenet坐标
mincost=>start: GenerateMinCostPath:通过撒点，五次曲线计算best路径
save=>start: best路径转化为frenet坐标系，并存储
reference=>start: 按照DiscretizedPath存入path_data(是reference的成员)
st->op->frenet->mincost->save->reference
```



#### cost计算方式
* 由类ComparableCost定义了cost的比较方式，比较内容如下：
``` C++
ComparableCost(const bool has_collision, const bool out_of_boundary,
                 const bool out_of_lane, const double safety_cost_,
                 const double smoothness_cost_)
```
包含了是否碰撞、是否超出边界、是否超出车道线、与障碍 边界的距离cost、平滑度cost（曲率）
具体的比较方式如下：
```C++
  int CompareTo(const ComparableCost &other) const {
    for (size_t i = 0; i < cost_items.size(); ++i) {
      if (cost_items[i]) { //对has_collision out_of_boundary out_of_lane进行检测
        if (other.cost_items[i]) { //如果this有碰撞、出界 压线，other也有，则比较其他
          continue;
        } else {  //如果this有碰撞、出界、压线，other没有，则返回1，表示other的cost更小
          return 1;
        }
      } else {
        if (other.cost_items[i]) { //如果this没有碰撞、出界、压线，other有，则返回-1，表示this的cost更小
          return -1;
        } else {
          continue;
        }
      }
    }
    constexpr double kEpsilon = 1e-12;
    const double diff = safety_cost + smoothness_cost - other.safety_cost -
                        other.smoothness_cost; // safety和smoothness的权重相同
    if (std::fabs(diff) < kEpsilon) {
      return 0;
    } else if (diff > 0) {
      return 1;
    } else {
      return -1;
    }
  }
```
* 具体的计算方式在TrajectoryCost::Calculate：
1.smoothness_cost 的计算方式是在：
```C++
ComparableCost TrajectoryCost::CalculatePathCost(
    const QuinticPolynomialCurve1d &curve, const double start_s,
    const double end_s, const uint32_t curr_level, const uint32_t total_level) 
```
2.obstacle_cost的计算方式
```C++
//静态障碍物：
ComparableCost TrajectoryCost::CalculateStaticObstacleCost(
    const QuinticPolynomialCurve1d &curve, const double start_s,
    const double end_s)
//通过几何特征确定是否碰撞，但是默认车都是与车道方向平行。
ComparableCost TrajectoryCost::GetCostFromObsSL(    
      bool no_overlap = ((adc_front_s < obs_sl_boundary.start_s() ||
                      adc_end_s > obs_sl_boundary.end_s()) ||  // longitudinal
                     (adc_left_l + FLAGS_static_decision_nudge_l_buffer < //+0.5
                          obs_sl_boundary.start_l() ||
                      adc_right_l - FLAGS_static_decision_nudge_l_buffer >
                          obs_sl_boundary.end_l()));  // lateral

  if (!no_overlap) {
    obstacle_cost.cost_items[ComparableCost::HAS_COLLISION] = true;
  }
  )
```

```C++
ComparableCost TrajectoryCost::CalculateDynamicObstacleCost(
    const QuinticPolynomialCurve1d &curve, const double start_s,
    const double end_s) const
    //通过时间个期望速度确定路径是否会碰撞，并增加obstacle_cost.safety_cost
```
#### return
返回状态，同时返回pathdata（其中包含五次多项式得到的曲线，按照间隔config_.path_resolution()取到的，在FrenetFrame坐标系下）

#### 缺点
* 路径不能够设定曲率限制，只能在可选的路径中选择cost最小的路径。
#### 优点
* SL坐标系下考虑到二阶导数，即考虑到曲率变化率（车轮转角）
### PathDecider
PathDecider主要由Process函数组成，主要调用的函数为
```
apollo::common::Status PathDecider::Execute(
    Frame *frame, ReferenceLineInfo *reference_line_info) {
  Task::Execute(frame, reference_line_info);
  return Process(reference_line_info->path_data(),
                 reference_line_info->path_decision());
}

bool PathDecider::MakeStaticObstacleDecision(
    const PathData &path_data, PathDecision *const path_decision)
```
只做静态障碍物和行人、骑车的人的决策！！ 只做静态障碍物和行人、骑车的决策！！
主要只为静态障碍物做出是否停车或者向左、向右微调的决策。
如果是停车，需要给出停车时候距离前车的距离以及heading。
如果是向左、向右微调，则设定微调距离为定值：`FLAGS_nudge_distance_obstacle`
### DP_ST_SPEED_OPTIMIZER
![DP-speed.png](http://upload-images.jianshu.io/upload_images/11020056-a342bf179dc7da8e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
DpStSpeedOptimizer类继承自SpeedOptimizer，DpStSpeedOptimizer类继承自SpeedOptimizer继承自Task。其作用是利用动态规划的思想在ST坐标系中进行速度的规划。其主调函数也是继承的函数：
```C++
Status DpStSpeedOptimizer::Process(const SLBoundary& adc_sl_boundary,
                                   const PathData& path_data,
                                   const TrajectoryPoint& init_point,
                                   const ReferenceLine& reference_line,
                                   const SpeedData& reference_speed_data,
                                   PathDecision* const path_decision,
                                   SpeedData* const speed_data) {
                                   ...//省略一些初始化
CreateStBoundaryWithHistoryDecision(boundary_mapper, path_data,
                                           speed_data, path_decision)
StBoundaryMapper boundary_mapper.CreateStBoundary(path_decision).code()
SearchStGraph(boundary_mapper, path_data, speed_data, path_decision,
                       st_graph_debug)
                       ...
                                   }
```
#### 配置参数
```
    dp_st_speed_config {
        total_path_length: 149 //路径长度
        total_time: 40
        matrix_dimension_s: 150
        matrix_dimension_t: 40

        speed_weight: 0.0
        accel_weight: 10.0
        jerk_weight: 10.0
        obstacle_weight: 1.0
        reference_weight: 0.0
        go_down_buffer: 5.0
        go_up_buffer: 5.0

        default_obstacle_cost: 1e3

        default_speed_cost: 1.0e3
        exceed_speed_penalty: 10.0
        low_speed_penalty: 10.0
        keep_clear_low_speed_penalty: 10.0

        accel_penalty: 1.0
        decel_penalty: 1.0

        positive_jerk_coeff: 1.0
        negative_jerk_coeff: 1.0

        max_acceleration: 3.0
        max_deceleration: -4.0

        st_boundary_config { //St（距离--时间）图的配置参数
            boundary_buffer: 0.1
            high_speed_centric_acceleration_limit: 0.8
            low_speed_centric_acceleration_limit: 1.2
            high_speed_threshold: 12.58
            low_speed_threshold: 7.5
            minimal_kappa: 0.00001
            point_extension: 1.0
            lowest_speed: 2.5
            num_points_to_avg_kappa: 2
            static_obs_nudge_speed_ratio: 0.6
            dynamic_obs_nudge_speed_ratio: 0.8
            centri_jerk_speed_coeff: 1.0
        }
    }
```
读取最大最小加速度和减速度时读取的时候车辆配置参数
mkz_config.pb.txt：
```
  max_acceleration: 2.0
  max_deceleration: -6.0
```
#### 总体思路
- 首先读取dp_st_speed_config st_boundary_config两个配置文件；
- `path_decision->EraseStBoundaries();` 
- 其次`boundary_mapper.CreateStBoundary(path_decision)`是根据障碍可能与ST发生碰撞都设置为ST图中的边界。

* `SearchStGraph(boundary_mapper, path_data, speed_data, path_decision,
                     st_graph_debug)` 设置每个边界的性质为YIELD OVERTAKE FOLLOW STOP之一，boundary_mapper.GetSpeedLimits(path_decision->path_obstacles(),&speed_limit)根据参考线设定的速度上限和路径曲率求得加速度限制和速度限制； 
                     -- 在ST图中找到最优速度曲线，存入speed_data，这里的
#### 输入输出
##### input
```
auto ret = Process(
      reference_line_info->AdcSlBoundary(), reference_line_info->path_data(),
      frame->PlanningStartPoint(), reference_line_info->reference_line(),
      *reference_line_info->mutable_speed_data(),
      reference_line_info->path_decision(),
      reference_line_info->mutable_speed_data());
      
Status DpStSpeedOptimizer::Process(const SLBoundary& adc_sl_boundary,
                                   const PathData& path_data,//DP path得到的数据
                                   const TrajectoryPoint& init_point,//初始位置
                                   const ReferenceLine& reference_line,//参考线
                                   const SpeedData& reference_speed_data,//参考线的初始速度（未使用）
                                   PathDecision* const path_decision,//决策信息，存储每个障碍的决策：避让 超车 跟随等信息。 
                                   SpeedData* const speed_data)//参考线的初始速度
```
##### output
```
reference_line_info->path_decision()//存储了St图的信息，某个障碍对应的ST图的上限 下限
reference_line_info->mutable_speed_data() //存储Dp规划出来的速度信息
```
### SPEED_DECIDER
根据当前路线中障碍物与车辆的位置，作出跟随(Follow)、避让(Yield)、超车(Overtake)、停车(Stop)．
程序执行顺序：
```flow
st=>start: MakeObjectDecision(reference_line_info->speed_data(),reference_line_info->path_decision()
op=>start: 根据每个障碍物做出速度决策
st->op

```
### QP_SPLINE_ST_SPEED_OPTIMIZER
#### 二次规划问题
带有二次型目标函数和约束条件的最优化问题。
##### [基础概念](http://blog.csdn.net/jbb0523/article/details/50598523)
二次型：函数中最高次为2次的函数。用矩阵可以记为$f=x^T \cdot A \cdot x$;
正定矩阵 Positive Definite Matrix：，设在二次型$f=x^T \cdot A \cdot x$中，对于任何$x\ne0$,都有$f(x)>0$,称$f$为正定二次型，称对称矩阵A为正定的;如果对于任何$x\ne0$,都有$f(x)<0$,称$f$为负定二次型，称对称矩阵A为负定的。
Hesse矩阵（海塞矩阵）：常用于牛顿法解决最优化问题。是一个类似与雅可比矩阵的概念，不过其是二阶导数的矩阵，但是雅可比矩阵是一阶导数的矩阵。如果函数f是连续的，则它的Hesse矩阵一定是对称阵。 得到函数f的Hesse矩阵有什么用呢？Hesse可以用于多元函数极值的判定。
[在清华的课程中也讲到了二次规划的问题](http://mathexp.math.tsinghua.edu.cn/optiweb/review/2_5qpam.htm)：
![QP-define.png](https://upload-images.jianshu.io/upload_images/11020056-51f74e99b101e037.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-matlab.png](https://upload-images.jianshu.io/upload_images/11020056-3c02f04ee56a6ee8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-solution.png](https://upload-images.jianshu.io/upload_images/11020056-a8fc08cfad7251b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

##### [求解形式](http://mathexp.math.tsinghua.edu.cn/optiweb/review/2_5qpam.htm)：
![QP-speed.png](http://upload-images.jianshu.io/upload_images/11020056-42e247c0e362b837.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

读取的最大加速度和最大减速度为qp_st_speed_config.txt文件中的配置：
```
        preferred_max_acceleration: 2.5 //推荐参数
        preferred_min_deceleration: -3.3
        max_acceleration: 3.0 //本来参数
        min_deceleration: -4.5
```
#### 输入输出
##### input
输入与Dp相同，都是继承speed_optimezer.cc。具体输入如下：
```
  auto ret = Process(
      reference_line_info->AdcSlBoundary(), reference_line_info->path_data(),
      frame->PlanningStartPoint(), reference_line_info->reference_line(),
      *reference_line_info->mutable_speed_data(),
      reference_line_info->path_decision(),
      reference_line_info->mutable_speed_data());
      
Status QpSplineStSpeedOptimizer::Process(const SLBoundary& adc_sl_boundary,
                                         const PathData& path_data,
                                         const TrajectoryPoint& init_point,
                                         const ReferenceLine& reference_line,
                                         const SpeedData& reference_speed_data,
                                         PathDecision* const path_decision,
                                         SpeedData* const speed_data)
```
同时的输入还有task父类的reference_line_info_
#### output
与Dp的输出相同，是对每个障碍的St建立和速度data。
1.reference_line_info->path_decision()//存储了St图的信息，某个障碍对应的ST图的上限 下限
2. reference_line_info->mutable_speed_data() //存储Dp规划出来的速度信息
#### 总体思路
1、检验是否达到终点
2、清理ST图中的边，并重新建立St图 `boundary_mapper.CreateStBoundary(path_decision)`，这一步骤与Dp速度规划的相同。应该是重复计算了的。
3、获取速度限制，`boundary_mapper.GetSpeedLimits(path_decision->path_obstacles(),
                                     &speed_limits)`这也和Dp规划相同；
4、图搜索：先用推荐的最大和最小减速度搜索，如果没有搜索到结果就把加速度和减速度变成最大和最小加减速度；如果还搜索不到路径就使用备用方案：分段st qp规划
二次样条曲线速度优化器。
1 Definition
在QP-Spline-Path找到路径后，将路径上的所有障碍和ADV放在ST图中，表示的是路径上沿着路径的情况。速度优化的任务是找到一条无碰撞的舒适路径。
使用样条曲线替代速度方程，使用ST图中的一系列S-T点代表最佳速度点。使用二次编程的方法得到最佳速度配置。[二次编程的定义](http://blog.csdn.net/jbb0523/article/details/50598641)如下：第一个为目标函数，后面为约束条件。

$$ minimize \frac{1}{2} \cdot x^T \cdot H \cdot x + f^T \cdot x \\ s.t. LB \leq x \leq UB \\ A_{eq}x = b_{eq} \\ Ax \leq b $$
式中H表示Hesse矩阵
2 Objective function
1.1 Get spline segments
将ST分为n端，每一段都是用一个多项式表示。

1.2 Define function for each spline segment
每一段i都有一个参考线累计距离 $d_i$,每一段默认都是五次多项式。每个多项式可以通过配置参数进行调整。
$$ s = f_i(t) = a_{0i} + a_{1i} \cdot t + a_{2i} \cdot t^2 + a_{3i} \cdot t^3 + a_{4i} \cdot t^4 + a_{5i} \cdot t^5 $$

1.3 Define objective function of optimization for each segment
平滑度评估项$cost_1$

$$ cost_1 = \sum_{i=1}^{n} \Big( w_1 \cdot \int\limits_{0}^{d_i} (f_i')^2(s) ds + w_2 \cdot \int\limits_{0}^{d_i} (f_i'')^2(s) ds + w_3 \cdot \int\limits_{0}^{d_i} (f_i^{\prime\prime\prime})^2(s) ds \Big) $$
$cost_2$ 表示最终ST轨迹与巡航轨迹（目标速度，即速度上限）的偏差。

$$ cost_2 = \sum_{i=1}^{n}\sum_{j=1}^{m}\Big(f_i(t_j)- s_j\Big)^2 $$

$cost_3$表示第一个ST路径和接下来的ST路径差。
$$ cost_3 = \sum_{i=1}^{n}\sum_{j=1}^{o}\Big(f_i(t_j)- s_j\Big)^2 $$

目标函数可以表示为：
$$ cost = cost_1 + cost_2 + cost_3 $$

3 约束
3.1 初始位置约束
设初始位置为($t0$, $s0$),$s0$在规划的路径$f_i(t)$, $f'i(t)$， $f_i(t)''$中（位置 速度 加速度）。将这些约束加入到二次规划等式约束：

$$ A_{eq}x = b_{eq} $$

3.2 单调约束
车辆只能正向行驶，不能够逆向行驶。
在路径中采样m个点，对于每一个$j， j-1$ 点对 ($j\in[1,...,m]$):
如果两个点在同一样条曲线 $k$中，则需要满足:

$$ \begin{vmatrix} 1 & t_j & t_j^2 & t_j^3 & t_j^4&t_j^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_k \\ b_k \\ c_k \\ d_k \\ e_k \\ f_k \end{vmatrix} > \begin{vmatrix} 1 & t_{j-1} & t_{j-1}^2 & t_{j-1}^3 & t_{j-1}^4&t_{j-1}^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{k} \\ b_{k} \\ c_{k} \\ d_{k} \\ e_{k} \\ f_{k} \end{vmatrix} $$

如果分别是两个样条 $k$ 以及 $l$中，需要满足:
$$ \begin{vmatrix} 1 & t_j & t_j^2 & t_j^3 & t_j^4&t_j^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_k \\ b_k \\ c_k \\ d_k \\ e_k \\ f_k \end{vmatrix} > \begin{vmatrix} 1 & t_{j-1} & t_{j-1}^2 & t_{j-1}^3 & t_{j-1}^4&t_{j-1}^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{l} \\ b_{l} \\ c_{l} \\ d_{l} \\ e_{l} \\ f_{l} \end{vmatrix} $$

3.3 联合曲率约束
这个约束是设计来平滑两个样条结合处的。假定有两段分别为 $seg_k$ 和 $seg_{k+1}$是相互连接的，两段共同的点为$s_k$，约束可表示为：
$$ f_k(t_k) = f_{k+1} (t_0) $$
即上一段曲线的结尾要与下一段曲线的开头相等。
表示为矩阵:
$$ \begin{vmatrix} 1 & t_k & t_k^2 & t_k^3 & t_k^4&t_k^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{k0} \\ a_{k1} \\ a_{k2} \\ a_{k3} \\ a_{k4} \\ a_{k5} \end{vmatrix} = \begin{vmatrix} 1 & t_{0} & t_{0}^2 & t_{0}^3 & t_{0}^4&t_{0}^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{k+1,0} \\ a_{k+1,1} \\ a_{k+1,2} \\ a_{k+1,3} \\ a_{k+1,4} \\ a_{k+1,5} \end{vmatrix} $$

变换
$$ \begin{vmatrix} 1 & t_k & t_k^2 & t_k^3 & t_k^4&t_k^5 & -1 & -t_{0} & -t_{0}^2 & -t_{0}^3 & -t_{0}^4&-t_{0}^5\\ \end{vmatrix} \cdot \begin{vmatrix} a_{k0} \\ a_{k1} \\ a_{k2} \\ a_{k3} \\ a_{k4} \\ a_{k5} \\ a_{k+1,0} \\ a_{k+1,1} \\ a_{k+1,2} \\ a_{k+1,3} \\ a_{k+1,4} \\ a_{k+1,5} \end{vmatrix} = 0 $$

The result is $t_0$ = 0 in the equation.
同理，不仅需要位置相同，一阶导、二阶导、三阶导都要相同：

$$ f'_k(t_k) = f'_{k+1} (t_0) \\ f''_k(t_k) = f''_{k+1} (t_0) \\ f'''_k(t_k) = f'''_{k+1} (t_0) $$

3.4 边界约束下的采样点
在路径中采样m个点，对这些点进行障碍边界检测，将这个约束转化为QR中小于等于的约束：
$$ Ax \leq b $$
首先根据道路宽度和周围的障碍找到点($s_j$, $l_j$)的下界： $l_{lb,j}$ ：

$$ \begin{vmatrix} 1 & t_0 & t_0^2 & t_0^3 & t_0^4&t_0^5 \\ 1 & t_1 & t_1^2 & t_1^3 & t_1^4&t_1^5 \\ ...&...&...&...&...&... \\ 1 & t_m & t_m^2 & t_m^3 & t_m^4&t_m^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_i \\ b_i \\ c_i \\ d_i \\ e_i \\ f_i \end{vmatrix} \leq \begin{vmatrix} l_{lb,0}\\ l_{lb,1}\\ ...\\ l_{lb,m}\\ \end{vmatrix} $$
然后同理找到上界：

$$ \begin{vmatrix} 1 & t_0 & t_0^2 & t_0^3 & t_0^4&t_0^5 \\ 1 & t_1 & t_1^2 & t_1^3 & t_1^4&t_1^5 \\ ...&...&...&...&...&... \\ 1 & t_m & t_m^2 & t_m^3 & t_m^4&t_m^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_i \\ b_i \\ c_i \\ d_i \\ e_i \\ f_i \end{vmatrix} \leq -1 \cdot \begin{vmatrix} l_{ub,0}\\ l_{ub,1}\\ ...\\ l_{ub,m}\\ \end{vmatrix} $$

3.5 速度边界约束
也考虑到了速度约束。
在st曲线中进行采样m个点，对每个点根据速度限制设置上界和下界： $v{ub,j}$ 和 $v{lb,j}$ ，表示为：

$$ f'(t_j) \geq v_{lb,j} $$

即
$$ \begin{vmatrix} 0& 1 & t_0 & t_0^2 & t_0^3 & t_0^4 \\ 0 & 1 & t_1 & t_1^2 & t_1^3 & t_1^4 \\ ...&...&...&...&...&... \\ 0& 1 & t_m & t_m^2 & t_m^3 & t_m^4 \\ \end{vmatrix} \cdot \begin{vmatrix} a_i \\ b_i \\ c_i \\ d_i \\ e_i \\ f_i \end{vmatrix} \geq \begin{vmatrix} v_{lb,0}\\ v_{lb,1}\\ ...\\ v_{lb,m}\\ \end{vmatrix} $$

以及
$$ f'(t_j) \leq v_{ub,j} $$

即
$$ \begin{vmatrix} 0& 1 & t_0 & t_0^2 & t_0^3 & t_0^4 \\ 0 & 1 & t_1 & t_1^2 & t_1^3 & t_1^4 \\ ...&...&...&...&...&... \\ 0 &1 & t_m & t_m^2 & t_m^3 & t_m^4 \\ \end{vmatrix} \cdot \begin{vmatrix} a_i \\ b_i \\ c_i \\ d_i \\ e_i \\ f_i \end{vmatrix} \leq \begin{vmatrix} v_{ub,0}\\ v_{ub,1}\\ ...\\ v_{ub,m}\\ \end{vmatrix} $$

### QP_path
目前暂时没有用。
1. Objective function
1.1 Get path length
Path is defined in station-lateral coordination system. The s range from vehicle's current position to default planing path length.

1.2 Get spline segments
Split the path into n segments. each segment trajectory is defined by a polynomial.

1.3 Define function for each spline segment
Each segment i has accumulated distance $d_i$ along reference line. The trajectory for the segment is defined as a polynomial of degree five by default.

$$ l = f_i(s) = a_{i0} + a_{i1} \cdot s + a_{i2} \cdot s^2 + a_{i3} \cdot s^3 + a_{i4} \cdot s^4 + a_{i5} \cdot s^5 (0 \leq s \leq d_{i}) $$

1.4 Define objective function of optimization for each segment
$$ cost = \sum_{i=1}^{n} \Big( w_1 \cdot \int\limits_{0}^{d_i} (f_i')^2(s) ds + w_2 \cdot \int\limits_{0}^{d_i} (f_i'')^2(s) ds + w_3 \cdot \int\limits_{0}^{d_i} (f_i^{\prime\prime\prime})^2(s) ds \Big) $$

1.5 Convert the cost function to QP formulation
QP formulation:

$$ \begin{aligned} minimize & \frac{1}{2} \cdot x^T \cdot H \cdot x + f^T \cdot x \\ s.t. \qquad & LB \leq x \leq UB \\ & A_{eq}x = b_{eq} \\ & Ax \geq b \end{aligned} $$

Below is the example for converting the cost function into the QP formulaiton.
$$ f_i(s) ＝ \begin{vmatrix} 1 & s & s^2 & s^3 & s^4 & s^5 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$

And

$$ f_i'(s) = \begin{vmatrix} 0 & 1 & 2s & 3s^2 & 4s^3 & 5s^4 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$

And

$$ f_i'(s)^2 = \begin{vmatrix} a_{i0} & a_{i1} & a_{i2} & a_{i3} & a_{i4} & a_{i5} \end{vmatrix} \cdot \begin{vmatrix} 0 \\ 1 \\ 2s \\ 3s^2 \\ 4s^3 \\ 5s^4 \end{vmatrix} \cdot \begin{vmatrix} 0 & 1 & 2s & 3s^2 & 4s^3 & 5s^4 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$

then we have,
$$ \int\limits_{0}^{d_i} f_i'(s)^2 ds ＝ \int\limits_{0}^{d_i} \begin{vmatrix} a_{i0} & a_{i1} & a_{i2} & a_{i3} & a_{i4} & a_{i5} \end{vmatrix} \cdot \begin{vmatrix} 0 \\ 1 \\ 2s \\ 3s^2 \\ 4s^3 \\ 5s^4 \end{vmatrix} \cdot \begin{vmatrix} 0 & 1 & 2s & 3s^2 & 4s^3 & 5s^4 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} ds $$

extract the const outside the integration, we have,

$$ \int\limits_{0}^{d_i} f'(s)^2 ds ＝ \begin{vmatrix} a_{i0} & a_{i1} & a_{i2} & a_{i3} & a_{i4} & a_{i5} \end{vmatrix} \cdot \int\limits_{0}^{d_i} \begin{vmatrix} 0 \\ 1 \\ 2s \\ 3s^2 \\ 4s^3 \\ 5s^4 \end{vmatrix} \cdot \begin{vmatrix} 0 & 1 & 2s & 3s^2 & 4s^3 & 5s^4 \end{vmatrix} ds \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$ $$ ＝\begin{vmatrix} a_{i0} & a_{i1} & a_{i2} & a_{i3} & a_{i4} & a_{i5} \end{vmatrix} \cdot \int\limits_{0}^{d_i} \begin{vmatrix} 0 & 0 &0&0&0&0\\ 0 & 1 & 2s & 3s^2 & 4s^3 & 5s^4\\ 0 & 2s & 4s^2 & 6s^3 & 8s^4 & 10s^5\\ 0 & 3s^2 & 6s^3 & 9s^4 & 12s^5&15s^6 \\ 0 & 4s^3 & 8s^4 &12s^5 &16s^6&20s^7 \\ 0 & 5s^4 & 10s^5 & 15s^6 & 20s^7 & 25s^8 \end{vmatrix} ds \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$

Finally, we have

$$ \int\limits_{0}^{d_i} f'_i(s)^2 ds =\begin{vmatrix} a_{i0} & a_{i1} & a_{i2} & a_{i3} & a_{i4} & a_{i5} \end{vmatrix} \cdot \begin{vmatrix} 0 & 0 & 0 & 0 &0&0\\ 0 & d_i & d_i^2 & d_i^3 & d_i^4&d_i^5\\ 0& d_i^2 & \frac{4}{3}d_i^3& \frac{6}{4}d_i^4 & \frac{8}{5}d_i^5&\frac{10}{6}d_i^6\\ 0& d_i^3 & \frac{6}{4}d_i^4 & \frac{9}{5}d_i^5 & \frac{12}{6}d_i^6&\frac{15}{7}d_i^7\\ 0& d_i^4 & \frac{8}{5}d_i^5 & \frac{12}{6}d_i^6 & \frac{16}{7}d_i^7&\frac{20}{8}d_i^8\\ 0& d_i^5 & \frac{10}{6}d_i^6 & \frac{15}{7}d_i^7 & \frac{20}{8}d_i^8&\frac{25}{9}d_i^9 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} $$

Please notice that we got a 6 x 6 matrix to represent the derivative cost of 5th order spline.

Similar deduction can also be used to calculate the cost of second and third order derivatives.

2 Constraints
2.1 The init point constraints
Assume that the first point is ($s_0$, $l_0$), ($s_0$, $l'_0$) and ($s_0$, $l''_0$), where $l_0$ , $l'_0$ and $l''_0$ is the lateral offset and its first and second derivatives on the init point of planned path, and are calculated from $f_i(s)$, $f'_i(s)$, and $f_i(s)''$.

Convert those constraints into QP equality constraints, using:

$$ A_{eq}x = b_{eq} $$

Below are the steps of conversion.
$$ f_i(s_0) = \begin{vmatrix} 1 & s_0 & s_0^2 & s_0^3 & s_0^4&s_0^5 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5}\end{vmatrix} = l_0 $$

And
$$ f'_i(s_0) = \begin{vmatrix} 0& 1 & 2s_0 & 3s_0^2 & 4s_0^3 &5 s_0^4 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} = l'_0 $$

And
$$ f''_i(s_0) = \begin{vmatrix} 0&0& 2 & 3\times2s_0 & 4\times3s_0^2 & 5\times4s_0^3 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} = l''_0 $$

where i is the index of the segment that contains the $s_0$.
2.2 The end point constraints
Similar to the init point, the end point $(s_e, l_e)$ is known and should produce the same constraint as described in the init point calculations.

Combine the init point and end point, and show the equality constraint as:

$$ \begin{vmatrix} 1 & s_0 & s_0^2 & s_0^3 & s_0^4&s_0^5 \\ 0&1 & 2s_0 & 3s_0^2 & 4s_0^3 & 5s_0^4 \\ 0& 0&2 & 3\times2s_0 & 4\times3s_0^2 & 5\times4s_0^3 \\ 1 & s_e & s_e^2 & s_e^3 & s_e^4&s_e^5 \\ 0&1 & 2s_e & 3s_e^2 & 4s_e^3 & 5s_e^4 \\ 0& 0&2 & 3\times2s_e & 4\times3s_e^2 & 5\times4s_e^3 \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} = \begin{vmatrix} l_0\\ l'_0\\ l''_0\\ l_e\\ l'_e\\ l''_e\\ \end{vmatrix} $$

2.3 Joint smoothness constraints
This constraint is designed to smooth the spline joint. Assume two segments $seg_k$ and $seg_{k+1}$ are connected, and the accumulated s of segment $seg_k$ is $s_k$. Calculate the constraint equation as:

$$ f_k(s_k) = f_{k+1} (s_0) $$

Below are the steps of the calculation.
$$ \begin{vmatrix} 1 & s_k & s_k^2 & s_k^3 & s_k^4&s_k^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{k0} \\ a_{k1} \\ a_{k2} \\ a_{k3} \\ a_{k4} \\ a_{k5} \end{vmatrix} = \begin{vmatrix} 1 & s_{0} & s_{0}^2 & s_{0}^3 & s_{0}^4&s_{0}^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{k+1,0} \\ a_{k+1,1} \\ a_{k+1,2} \\ a_{k+1,3} \\ a_{k+1,4} \\ a_{k+1,5} \end{vmatrix} $$

Then
$$ \begin{vmatrix} 1 & s_k & s_k^2 & s_k^3 & s_k^4&s_k^5 & -1 & -s_{0} & -s_{0}^2 & -s_{0}^3 & -s_{0}^4&-s_{0}^5\\ \end{vmatrix} \cdot \begin{vmatrix} a_{k0} \\ a_{k1} \\ a_{k2} \\ a_{k3} \\ a_{k4} \\ a_{k5} \\ a_{k+1,0} \\ a_{k+1,1} \\ a_{k+1,2} \\ a_{k+1,3} \\ a_{k+1,4} \\ a_{k+1,5} \end{vmatrix} = 0 $$

Use $s_0$ = 0 in the equation.
Similarly calculate the equality constraints for:

$$ f'_k(s_k) = f'_{k+1} (s_0) \\ f''_k(s_k) = f''_{k+1} (s_0) \\ f'''_k(s_k) = f'''_{k+1} (s_0) $$

2.4 Sampled points for boundary constraint
Evenly sample m points along the path, and check the obstacle boundary at those points. Convert the constraint into QP inequality constraints, using:

$$ Ax \geq b $$

First find the lower boundary $l_{lb,j}$ at those points $(s_j, l_j)$ and $j\in[0, m]$ based on the road width and surrounding obstacles. Calculate the inequality constraints as:
$$ \begin{vmatrix} 1 & s_0 & s_0^2 & s_0^3 & s_0^4&s_0^5 \\ 1 & s_1 & s_1^2 & s_1^3 & s_1^4&s_1^5 \\ ...&...&...&...&...&... \\ 1 & s_m & s_m^2 & s_m^3 & s_m^4&s_m^5 \\ \end{vmatrix} \cdot \begin{vmatrix}a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} \geq \begin{vmatrix} l_{lb,0}\\ l_{lb,1}\\ ...\\ l_{lb,m}\\ \end{vmatrix} $$

Similarly, for the upper boundary $l_{ub,j}$, calculate the inequality constraints as:

$$ \begin{vmatrix} -1 & -s_0 & -s_0^2 & -s_0^3 & -s_0^4&-s_0^5 \\ -1 & -s_1 & -s_1^2 & -s_1^3 & -s_1^4&-s_1^5 \\ ...&...-&...&...&...&... \\ -1 & -s_m & -s_m^2 & -s_m^3 & -s_m^4&-s_m^5 \\ \end{vmatrix} \cdot \begin{vmatrix} a_{i0} \\ a_{i1} \\ a_{i2} \\ a_{i3} \\ a_{i4} \\ a_{i5} \end{vmatrix} \geq -1 \cdot \begin{vmatrix} l_{ub,0}\\ l_{ub,1}\\ ...\\ l_{ub,m}\\ \end{vmatrix} $$
