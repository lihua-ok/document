# Math  

Author: QiYao

[TOC]

标签（空格分隔）： math curve

---

# 主要解析math文件夹中的各文件、函数内容以及相关调用的数学函数库
## QuinticPolynomialCurve1d
规划中用到的就是五次多项式曲线，五次多项式曲线指的是使用五次多项式来模拟车辆的运动轨迹。其公式表示如下：
$$ f(s) = a_{0} + a_{1} \cdot s + a_{2} \cdot s^2 + a_{3} \cdot s^3 + a_{4} \cdot s^4 + a_{5} \cdot s^5  $$
共需要计算的参数为6个：$a_{0},a_{1},a_{2},a_{3},a_{4},a_{5}$。
在输入起始点和目标点时可以得到参数，需要输入的起始点和目标点信息为：
```
/**
 * @brief QuinticPolynomialCurve1d::ComputeCoefficients
 * @param x0 初始位置坐标
 * @param dx0 初始位置x的导数
 * @param ddx0 初始位置x的二阶导数
 * @param x1  目标位置
 * @param dx1 一阶导数
 * @param ddx1 二阶导数
 * @param p 长度
 即表示初始位置为(x0, 0)，目标位置为(x1, p),
 */
 void QuinticPolynomialCurve1d::ComputeCoefficients(
    const double x0, const double dx0, const double ddx0, const double x1,
    const double dx1, const double ddx1, const double p) {
      CHECK_GT(p, 0.0);
      
  coef_[0] = x0; //可以通过多项式s=0求得
  coef_[1] = dx0; //通过多项式求导得到
  coef_[2] = ddx0 / 2.0;

  const double p2 = p * p;
  const double p3 = p * p2;

  // the direct analytical method is at least 6 times faster than using matrix
  // inversion.
  const double c0 = (x1 - 0.5 * p2 * ddx0 - dx0 * p - x0) / p3;
  const double c1 = (dx1 - ddx0 * p - dx0) / p2;
  const double c2 = (ddx1 - ddx0) / p;

  coef_[3] = 0.5 * (20.0 * c0 - 8.0 * c1 + c2);
  coef_[4] = (-15.0 * c0 + 7.0 * c1 - c2) / p;
  coef_[5] = (6.0 * c0 - 3.0 * c1 + 0.5 * c2) / p2;
}
```

## [qpoases](https://projects.coin-or.org/qpOASES)
这个库函数本来被设计来作为MPC的应用，但是也是一个可靠的QP算法求解方案。作为求解参数二次规划的有效集算法。
[使用说明书](https://www.coin-or.org/qpOASES/doc/3.2/manual.pdf)
### QP问题
#### 二次规划问题
带有二次型目标函数和约束条件的最优化问题。
##### [基础概念](http://blog.csdn.net/jbb0523/article/details/50598523)
二次型：函数中最高次为2次的函数。用矩阵可以记为$f=x^T \cdot A \cdot x$;
正定矩阵 Positive Definite Matrix：，设在二次型$f=x^T \cdot A \cdot x$中，对于任何$x\ne0$,都有$f(x)>0$,称$f$为正定二次型，称对称矩阵A为正定的;如果对于任何$x\ne0$,都有$f(x)<0$,称$f$为负定二次型，称对称矩阵A为负定的。
Hesse矩阵（海塞矩阵）：常用于牛顿法解决最优化问题。是一个类似与雅可比矩阵的概念，不过其是二阶导数的矩阵，但是雅可比矩阵是一阶导数的矩阵。如果函数f是连续的，则它的Hesse矩阵一定是对称阵。 得到函数f的Hesse矩阵有什么用呢？Hesse可以用于多元函数极值的判定。
[在清华的课程中也讲到了二次规划的问题](http://mathexp.math.tsinghua.edu.cn/optiweb/review/2_5qpam.htm)：
![QP-define.png](https://upload-images.jianshu.io/upload_images/11020056-51f74e99b101e037.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-matlab.png](https://upload-images.jianshu.io/upload_images/11020056-3c02f04ee56a6ee8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-solution.png](https://upload-images.jianshu.io/upload_images/11020056-a8fc08cfad7251b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 使用qpOASES求解步骤
以下文档都是从3.2版本的用户说明书中提取的简要部分，如果需要详细的还请阅读说明书：
1. 首先实例化一个QProblrm类的对象
`QProblem( int_t nV, int_t nC );` nv表示变量的个数，nc表示二次约束的的个数。
ex:`QProblem example( nV,nC );`
2. 初始化对象求求解。
![QP-init.png](https://upload-images.jianshu.io/upload_images/11020056-f0a577f3a1083ec4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
H：hessian矩阵  $H\in\mathbf{R^{nV \times nV}}$
g：梯度向量？$g\in\mathbf{R^{nV}}$
A：约束矩阵 $A\in\mathbf{R^{nC \times nV}}$
lb ub：上下限边界向量 $lb,ub\in\mathbf{R^{nV}}$
lbA ubA：上下限约束向量 $lb,ub\in\mathbf{R^{nC}}$。 **这个向量中不仅包含了不等式，也包括了等式的约束**,将上限和下限设置为相同数值即可表示为等式约束！
nWSR： 最大迭代次数
cputime：如果输入不为空，就会输出整个初始化和求解所花的时间。
如果那一项是没有的，就看可以传入一个空指针。解完后所有的向量需要自己释放内存。但是矩阵H A不需要。
### 使用qpOASES求解方式
QP问题分为许多种，有一些没有等式约束项，一些没有不等式约束项。因此在qpOASES中也有好几个方法来解决不同的问题，相比之下针对不同问题选择不同方法的效率会更高：

![QP-solving-variants.png](https://upload-images.jianshu.io/upload_images/11020056-90e24d0ebe3cfd65.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
针对不同的种类的详细用法可以参考`/examples/example**.cpp`
同时这个库可以解决线性规划，但是效率极低！
### qpOASES其余设置
>* 可以返回各项参数的数值
>* 可以定义 `Options myOptions;  qp.setOptions( myOptions );`设定需要设置的各参数
>* ex: ```
Options myOptions;
myOptions.setToMPC( );
myOptions.printLevel = PL_LOW;
qp.setOptions( myOptions );
```
>* 可以通过指定用于评估约束的函数来提升速度，但是可能这是naive的
>* 能够设置最大时间限制。因此由于时间测量的不准确，实际的总CPU时间可能比允许的CPU时间稍高一些。
