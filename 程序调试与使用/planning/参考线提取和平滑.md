﻿# 参考线提取和平滑

Author: QiYao

[TOC]

标签（空格分隔）： 未分类

---

## ReferenceLineSmoother
参考线平滑的类，是一个基类。继承其的类包括：QpSplineReferenceLineSmoother SpiralReferenceLineSmoother。
这部分可以参考[文档](https://github.com/ApolloAuto/apollo/blob/d246ef7c7787a14a3c07909ee04c4092134b423f/docs/specs/reference_line_smoother.md),我将文档中主要部分摘录，但是要注意的是这个文档比较早了，可能不一定全面。
参考线平滑过程：

>* 设置锚点GetAnchorPoints():按照均匀间隔（5）在参考线中设置锚点。GetAnchorPoint（）：根据距离获取参考线上的点，获取该点左右车道的宽度，如果车道太宽：如果是右侧通行，则距离左侧的距离增加为：半车宽+车宽×系数；如果是左侧通行，则距离左边的距离变为：总宽度-（半车宽+车宽×系数），相当于距离右边的距离为（半车宽+车宽×系数）,设置好锚点后能够将路径设置为偏向道路左侧或者右侧，留下足够的安全距离，但是这个路径是不满足车辆运动学约束的。如果左右边线是curb会留出smoother_config_.curb_shift()大小的偏差
left-handed:左侧通行
right-handed:右侧通行 
>* Sampling() 在锚点中按照长度计算出样条曲线的数量，求出第一个点的坐标。
>* AddConstraint() 调用Spline2dConstraint增加不能过偏离的bounding_box的不等式约束、第一个点的朝向要与锚点相同的等于约束、增加二阶导数的平滑约束（五次多项式的二阶导数）
>* AddKernel() 调用Spline2dKernel添加核
>* Solve() 调用Spline2d求解。
>* *smoothed_reference_line = ReferenceLine(ref_points); return true; 

Quadratic programming + Spline interpolation

## 1. Objective function

### 1.1 Segment routing path

Segment routing path into **n** segments. each segment trajectory is defined by two polynomials:

<p>
$$
x = f_i(t)
  = a_{i0} + a_{i1} * t + a_{i2} * t^2 + a_{i3} * t^3 + a_{i4} * t^4 + a_{i5} * t^5
$$
</p>

<p>
$$
y = g_i(t) = b_{i0} + b_{i1} * t + b_{i2} * t^2 + b_{i3} * t^3 + b_{i4} * t^4 + b_{i5} * t^5
$$
</p>

### 1.2 Define objective function of optimization for each segment

<p>
$$
cost = 
\sum_{i=1}^{n} 
\Big(
\int\limits_{0}^{t_i} (f_i''')^2(t) dt 
+ \int\limits_{0}^{t_i} (g_i''')^2(t) dt 
\Big)
$$
</p>

### 1.3 Convert the cost function to QP formulation

QP formulation:
<p>
$$
\frac{1}{2} \cdot x^T \cdot H \cdot x + f^T \cdot x 
\\
s.t. LB \leq x \leq UB
\\
A_{eq}x = b_{eq}
\\
Ax \leq b
$$
</p>


## 2 Constraints  


### 2.1 Joint smoothness  constraints

This constraint smoothes the spline joint.  Let's assume two segments, $seg_k$ and $seg_{k+1}$, are connected and the accumulated **s** of segment $seg_k$ is $s_k$. Calculate the constraint equation as: 
<p>
$$
f_k(s_k) = f_{k+1} (s_0)
$$
</p>
Similarly the formula works for the equality constraints, such as: 
<p>
$$
f'_k(s_k) = f'_{k+1} (s_0)
\\
f''_k(s_k) = f''_{k+1} (s_0)
\\
f'''_k(s_k) = f'''_{k+1} (s_0)
\\
g_k(s_k) = g_{k+1} (s_0)
\\
g'_k(s_k) = g'_{k+1} (s_0)
\\
g''_k(s_k) = g''_{k+1} (s_0)
\\
g'''_k(s_k) = g'''_{k+1} (s_0)
$$
</p>

### 2.2 Sampled points for boundary constraint

Evenly sample **m** points along the path and check the predefined boundaries at those points.  
<p>
$$
f_i(t_l) - x_l< boundary
\\
g_i(t_l) - y_l< boundary
$$
</p>
## QpSplineReferenceLineSmoother
使用二次规划样条的方法对参考线进行平滑。
## ReferenceLineProvider
### CreateReferenceLine
创造参考线，包括平滑参考线。第一次创造参考线时候调用`SmoothRouteSegment`->`SmoothRouteSegment`->`SmoothReferenceLine`
```C++
bool ReferenceLineProvider::SmoothReferenceLine(
    const ReferenceLine &raw_reference_line, ReferenceLine *reference_line) {
  if (!FLAGS_enable_smooth_reference_line) {
    *reference_line = raw_reference_line;
    return true;
  }
  // generate anchor points:
  std::vector<AnchorPoint> anchor_points;
  GetAnchorPoints(raw_reference_line, &anchor_points);
  smoother_->SetAnchorPoints(anchor_points);
  if (!smoother_->Smooth(raw_reference_line, reference_line)) {
    AERROR << "Failed to smooth reference line with anchor points";
    return false;
  }
  if (!IsReferenceLineSmoothValid(raw_reference_line, *reference_line)) {
    AERROR << "The smoothed reference line error is too large";
    return false;
  }
  return true;
}
```
平滑方法的选择是在planning_config.pb.txt中：`smoother_type: QP_SPLINE_SMOOTHER`
平滑方法现在共有三种，在planning/proto/planning_config.proto中定义：
```
message PlanningConfig {
  enum PlannerType {
    RTK = 0;
    EM = 1;  // expectation maximization
    LATTICE = 2;
  };
  optional PlannerType planner_type = 1 [default = EM];

  enum ReferenceLineSmootherType {
    SPIRAL_SMOOTHER = 1;     // generate by spiral path
    QP_SPLINE_SMOOTHER = 2;  // generate by quadratic programming
    TRACE_SMOOTHER = 3;      // leverage human driving trace
  }
  optional EMPlannerConfig em_planner_config = 2;
  optional ReferenceLineSmootherType smoother_type = 3
      [default = QP_SPLINE_SMOOTHER];
}
```
reference_line_ -> map_path_ -> reference_points_ ->[apollo::hdmap::MapPathPoint] -> lane_waypoints_ -> sl 经过平滑后的参考线的坐标。




 
