﻿
作者　　　曹波

Apollo中关于路径规划方面的内容小结
读者可以通过https://blog.csdn.net/ZYQ120625/article/details/79486026研究更详细的内容
一、Planning类
该类数据成员包括：规划者工厂类对象（planner_factory_）、规划配置类对象（config_）、高精地图类对象指针（hdmap_）、帧类对象指针（frame_）、规划者类对象指针（planner_）、可发布轨迹类对象指针　　（last_publishable_trajectory_）、计时器对象（timer_）；

1. 规划者工厂类对象（apollo::common::util::Factory<PlanningConfig::PlannerType, Planner> planner_factory_） 
　　　该对象的职能是注册所有的规划者类（目前注册了两个：RTK重放规划者RTKReplayPlanner，用于轨迹回放，无实际用途；最大期望规划者EMPlanner，可实用），并根据配置信息生成合适的规划者类对象（当前配置文件使用EMPlanner）

2. 规划配置类对象（PlanningConfig config_） 
该对象的职能是读取配置文件信息，给规划类对象提供合适的参数。

3. 高精地图类对象指针（hdmap::HDMap* hdmap_） 
该指针用于保存高精地图，给规划类对象提供导航地图及车辆定位点等信息。 

4. 帧类对象指针（std::unique_ptr<Frame> frame_） 
该指针用于保存帧（Frame）对象，规划类对象所有的决策都在该对象上完成。 

5. 规划者类对象指针（std::unique_ptr<Planner> planner_） 
该指针用于保存具体规划者对象。 

6. 可发布轨迹类对象指针（std::unique_ptr<PublishableTrajectory> last_publishable_trajectory_） 
该指针用于记录规划路径的位置和速度信息。
 
7. 计时器对象（ros::Timer timer_） 
该对象用于定期监控各类操作用户，调用相关处理函数。

重要成员函数

该类主要包含三个重载函数：Init、Start、Stop（均为基类ApolloApp的虚函数）和几个功能函数：InitFrame、RunOnce、Plan，其他函数不太重要。

1. Init函数 
该函数完成以下工作：获取高精地图(hdmap_ = HDMapUtil::BaseMapPtr())；读取配置文件获取相关参数(apollo::common::util::GetProtoFromFile(FLAGS_planning_config_file,&config_))；使用适配器管理者（apollo::common::adapterAdapterManager，Apollo项目内的所有模块如定位、底盘、预测、控制、规划等全部归其管理，类似于一个家庭管家）对象获取规划模块所需的定位、底盘、路径、交通灯探测、周边障碍物预测等信息；获取参考线（基于导航路径和当前位置计算得到，提供给Planning类决策的候选轨迹线)(reference_line_provider_ = std::unique_ptr<ReferenceLineProvider>(new ReferenceLineProvider(hdmap_, config_.smoother_type())))；注册具体的规划者类（目前注册RTKReplayPlanner及EMPlanner),(RegisterPlanners())；使用工厂创建模式生成具体的规划者类对象（通过查询配置文件获知，实际生成了EMPlanner类对象)；最后完成规划者类对象的初始化 planner_->Init(config_)。 

2. Start函数 
Start函数很简单，主要完成两个工作：一是启动参考线信息的获取；二是创建一个定时器，按给定间隔调用Planning::OnTimer函数，Planning::OnTimer函数的功能更为简单，即调用Planning::RunOnce完成实际的规划工作。 

3. Stop函数 
Stop函数的功能也很简单：一是结束参考线信息的获取；二是清空可发布轨迹类对象指针、帧类对象指针、规划者类对象指针。 

4. RunOnce函数 
RunOnce函数完成的具体工作。该函数首先调用AdapterManager类的相关函数来获取规划所需的所需的定位、底盘信息；接着基于上述信息，调用common::VehicleStateProvider::instance()->Update函数更新车辆自身的状态；之后调用Planning::InitFrame函数初始化规划帧；如果一切正常，就调用Planning::Plan函数执行实际的规划；最后调用Planning::PublishPlanningPb函数将计算出的规划路径发布给控制模块。另外，整个函数针对各种可能出现的错误均做出相应处理。 

5. InitFrame函数 
该函数功能比较简单，首先调用Frame::reset函数重新创建一个Frame类对象（如果之前不存在，直接创建；若已经存在一个，则先删除后创建），然后设置预测信息，最后调用Frame::Init函数完成当前帧的初始化。

6. Plan函数 
Plan函数是规划模块最核心的函数，用来完成实际的路径规划。该函数针对Frame类对象指针frame_提供的多条候选参考线信息：reference_line_info，调用规划者对象的Plan函数（planner_->Plan，当前实现就是调用EMPlanner::Plan函数）决策得到可用参考线信息集（可能不止一条）；接着调用Frame::FindDriveReferenceLineInfo函数从可用参考线信息集获取最优的参考线信息：best_reference_line；最后调用ReferenceLineInfo::ExportDecision函数，将best_reference_line转化可供控制模块使用的驾驶决策，包括：路径、速度等。整个函数存在很多输出调试信息的语句，注意不要被他们所干扰，影响对核心思路的理解。




期望最大规划者类（EMPlanner）分析

EMPlanner类是具体的规划实施类，它基于高精地图、导航路径及障碍物信息作出实际的驾驶决策，包括路径、速度等方面。首先使用DP（动态规划）方法确定初始的路径和速度，再利用QP（二次规划）方法进一步优化路径和速度，以得到一条更平滑的轨迹，既满足舒适性，又方便车辆操纵。

数据成员
该类数据成员包括：任务工厂类对象（task_factory_）、任务类对象数组（tasks_）。 
1. 任务工厂类对象（apollo::common::util::Factory<TaskType, Task> task_factory_） 
该对象的负责是注册所有的任务类，并根据配置信息生成合适的任务类对象。 

2. 任务类对象数组（std::vector<std::unique_ptr<Task>> tasks_） 
该数组的职能是保存各个任务类对象的智能指针。

重要成员函数
该类主要包含两个重载函数：Init、Plan（基类Planner的虚函数）和两个功能函数：GenerateInitSpeedProfile、GenerateSpeedHotStart，其他函数是辅助函数。 
1. Init函数 
该函数完成以下工作：注册各种任务类（目前已注册：交通决策器TrafficDecider、动态规划多项式路径优化器DpPolyPathOptimizer、路径决策器PathDecider、动态规划ST坐标速度优化器DpStSpeedOptimizer、速度决策器SpeedDecider、二次规划样条路径优化器QpSplinePathOptimizer、二次规划样条ST坐标速度优化器QpSplineStSpeedOptimizer）；使用工厂创建模式生成具体的任务类对象（通过查询配置文件modules/planning/conf/planning_config.pb.txt获知，任务类型包括： TRAFFIC_DECIDER、 DP_POLY_PATH_OPTIMIZER、PATH_DECIDER、DP_ST_SPEED_OPTIMIZER、SPEED_DECIDER、QP_SPLINE_ST_SPEED_OPTIMIZER，实际生成的任务对象分别为：TrafficDecider、DpPolyPathOptimizer、PathDecider 、DpStSpeedOptimizer、SpeedDecider、QpSplineStSpeedOptimizer类对象。注意：任务对象的创建可根据修改配置文件动态调整）；最后完成各种任务类对象的初始化。决策和优化器的继承关系图如下所示： 

2. Plan函数 
该函数首先对当前参考线信息（reference_line_info，即候选路径）作出判断，若其非向前直行，则加入10000的代价（cost）；接下来，基于起始点和当前参考线信息，调用EMPlanner::GenerateInitSpeedProfile函数生成初始速度，若失败则继续调用EMPlanner::GenerateSpeedHotStart生成初始速度；之后是整个函数的核心，针对当前帧frame和参考线信息reference_line_info，逐一调用任务类对象数组tasks_里的优化器（当前配置文件按照以下顺序使用以下决策或优化器：TrafficDecider、DpPolyPathOptimizer、PathDecider 、DpStSpeedOptimizer、SpeedDecider、QpSplineStSpeedOptimizer）进行最优估计，计算当前参考线的最优路径和速度。若针对当前参考线无法计算出最优路径和速度，则将其代价值设为无穷大，表示当前参考线无法使用；若对当前参考线可计算出最优路径和速度，则调用ReferenceLineInfo::CombinePathAndSpeedProfile函数合并路径和速度信息，最后调用ReferenceLineInfo::setTrajectory函数更新当前参考线的轨迹，返回给上一级Planning::Plan函数使用。整个函数存在很多输出调试信息的语句，注意不要被他们所干扰，影响对核心思路的理解。 

3. GenerateInitSpeedProfile函数 
该函数看上去很复杂，实际思想比较简单：首先获取上一帧last_frame的参考线信息last_reference_line_info；之后，对当前参考线信息reference_line_info调用ReferenceLineInfo::IsStartFrom函数来判断其是否起始于上一帧参考线信息last_reference_line_info，若不是直接返回，否则继续；接着，基于上一帧参考线的速度信息生成当前帧参考线的速度信息，注意这里用到了(x, y)坐标到(s, l)坐标的转换。 

4. GenerateSpeedHotStart函数 
该函数很简单，就是将初始点的速度限制在[5.0，FLAGS_planning_upper_speed_limit]范围内，并为当前帧参考线的初始速度信息。


帧类（Frame）分析

Frame类用于存储规划模块用到的各类信息，包括起始点、车辆状态、参考线信息列表（即多条候选路径）、驾驶参考线信息（即实际执行的驾驶路径）、障碍物预测等。所有决策都基于当前帧进行。
数据成员
1. 帧序号（uint32_t sequence_num_） 
记录当前帧的序号。

2. 高精地图类对象指针（hdmap::HDMap* hdmap_） 
保存高精地图，给规划类对象提供导航地图及车辆定位点等信息。

3. 轨迹起始点（common::TrajectoryPoint planning_start_point_） 
保存路径的起始点。

4. 车辆状态类对象（common::VehicleState vehicle_state_） 
存储车辆状态信息，包括位置、时间戳、姿态、速度、加速度、底盘及方位等。


5. 参考线信息列表（std::list<ReferenceLineInfo> reference_line_info_） 
存储多条候选路径。


6. 驾驶参考线信息指针（const ReferenceLineInfo *drive_reference_line_info_） 
记录实际执行驾驶路径。

7. 预测障碍物类对象（prediction::PredictionObstacles prediction_） 
存储车辆当前驾驶模式下，预测出现的障碍物。

8. 障碍物列表（prediction::PredictionObstacles prediction_） 
存储车辆周边所有的障碍物。

重要成员函数
该类主要包含如下成员函数：Init、FindDriveReferenceLineInfo，其他函数是辅助函数，如感兴趣可直接查看源代码。 
1. Init函数 
该函数被Planning::InitFrame函数调用，用于完成当前帧各类信息的初始化，包括获取高精地图、车辆状态，创建预测障碍、目的地障碍，查找碰撞障碍，初始化参考线信息等。 

2. FindDriveReferenceLineInfo函数 
该函数从可用的参考线信息列表中找到代价（cost）值最低的一条参考线信息，作为最终的驾驶路径。



ReferenceLineSmootherType：
SPIRAL_SMOOTHER　　螺旋平滑
QP_SPLINE_SMOOTHER　动态样条平滑
TRACE_SMOOTHER　跟踪平滑
ＤＰ路径算法：给予撒点网络，选取cost最低的路径
道路点采样：主车速度和位置路况信息
路径生成：平滑曲线连接，路径曲率连续且可导
最优化路径：最优化cost
Cost考虑：
与路径中心线偏差
路径曲率，曲率连续性
与障碍保持合理距离
路径曲率符合车辆物理特性



###＃TrajectoryPoint究竟指的是什么样的点，是规划后的的轨迹点？还是撒的点？还是路由寻径得到的点？？


###规划部分具体执行的过程
## 模块入口：该模块的主入口为：modules/planning/main.cc，展开该宏后的程序代码为

#define APOLLO_MAIN(APP)                                       \
  int main(int argc, char **argv) {                            \
    google::InitGoogleLogging(argv[0]);                        \
    google::ParseCommandLineFlags(&argc, &argv, true);         \
    signal(SIGINT, apollo::common::apollo_app_sigint_handler); \
    APP apollo_app_;                                           \
    FLAGS_log_dir = "../log/";                                  \
    std::string home = FLAGS_log_dir + apollo_app_.Name() + "_";\
    std::string info_log = home + "INFO_";\
    google::SetLogDestination(google::INFO, info_log.c_str());\
    std::string warning_log = home + "WARNING_";\
    google::SetLogDestination(google::WARNING, warning_log.c_str());\
    std::string error_log = home + "ERROR_";\
    google::SetLogDestination(google::ERROR, error_log.c_str());\
    std::string fatal_log = home + "FATAL_";\
    google::SetLogDestination(google::FATAL, fatal_log.c_str());\
    FLAGS_stderrthreshold=google::INFO;                         \
    FLAGS_v = 5;                                                \
    ros::init(argc, argv, apollo_app_.Name());                 \
    apollo_app_.Spin();                                        \
    return 0;                                                  \
  }
## 这里的Main函数完成以下工作：
　　始化Google日志工具，使用Google命令行解析工具解析相关参数，注册接收中止信号“SIGINT”的处理函数：apollo::common::apollo_app_sigint_handler（该函数的功能十分简单，就是收到中止信号“SIGINT”后，调用ros::shutdown()关闭ROS），创建apollo::planning::Planning对象：apollo_app_，初始化ROS环境，调用apollo_app_.Spin()数开始消息处理循环

##ApolloApp::Spin()函数内部
  首先创建ros::AsyncSpinner对象spinner，监控用户操作。之后调用虚函数：Init()（实际调用apollo::planning::Planning::Init()）执行初始化

##在apollo::planning::Planning::Init()函数内部
  首先获取高精地图指针，之后执行适配器管理者（AdapterManager）对象的初始化状态，接着检查AdapterManager里定位适配器（AdapterManager::GetLocalization()）、底盘适配器（AdapterManager::GetChassis()、路由寻径响应适配器（AdapterManager::GetRoutingResponse()、路由寻径请求适配器（AdapterManager::GetRoutingRequest()）的初始化状态，若启用预测（FLAGS_enable_prediction）则继续检查预测适配器（AdapterManager::GetPrediction()的初始化状态，若启用交通灯（FLAGS_enable_traffic_light）则继续检查交通灯适配器（AdapterManager::GetTrafficLightDetection()）的初始化状态。接下来，执行参考线提供者（所谓参考线就是一条候选路径）的初始化。此后调用Planning::RegisterPlanners()将RTKReplayPlanner、EMPlanner对象的创建函数注册到计划者工厂类对象planner_factory_中。另外通过读取配置文件中给定的计划者类型，使用planner_factory_.CreateObject(config_.planner_type())动态生成所需的计划者对象（查询配置文件得知，实际生成EMPlanner对象）。最后执行计划者对象的初始化：planner_->Init(config_)

##在上述过程中，主要执行了规划类（Planning）中的Init函数，接下来就是要进行RunOnce函数
##RunOnce函数完成的具体工作:
  首先调用AdapterManager类的相关函数来获取规划所需的所需的定位、底盘信息；接着基于上述信息，调用common::VehicleStateProvider::instance()->Update函数更新车辆自身的状态；之后调用Planning::InitFrame函数初始化规划帧；如果一切正常，就调用Planning::Plan函数执行实际的规划；最后调用Planning::PublishPlanningPb函数将计算出的规划路径发布给控制模块。另外，整个函数针对各种可能出现的错误均做出相应处理

##RunOnce函数中，调用了规划类（Planning）中Plan函数，这里的Plan函数：
　　是规划模块最核心的函数，用来完成实际的路径规划。该函数针对Frame类对象指针frame_提供的多条候选参考线信息：reference_line_info，调用规划者对象的Plan函数（planner_->Plan，当前实现就是调用EMPlanner::Plan函数）决策得到可用参考线信息集（可能不止一条）；接着调用Frame::FindDriveReferenceLineInfo函数从可用参考线信息集获取最优的参考线信息：best_reference_line；最后调用ReferenceLineInfo::ExportDecision函数，将best_reference_line转化可供控制模块使用的驾驶决策，包括：路径、速度等

##在Plan函数中
会调用注册的具体的planner_的Plan函数（目前所调用的是EMPlaner的Plan函数）实施最终的路径规划与速度规划
进入到EMPlaner的Plan函数中后，会调用Task::Execute函数（根据配置文件决定具体调用那些决策器和优化器。在目前的版本里边，按顺序分别调用TrafficDecider::Execute、DpPolyPathOptimizer::Execute,PathDecider::Execute,DpStSpeedOptimizer::Execute,
SpeedDecider::Execute,QpSplineStSpeedOptimizer::Execute）等几个任务情况





