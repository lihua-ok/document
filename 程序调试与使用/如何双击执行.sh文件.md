﻿# 如何双击执行.sh文件
###作者：谢枫
标签（教程）： 教程

---

通常情况下，当我们写好脚本程序后，需要在脚本所在文件夹下打开终端并输入./script_name.sh执行脚本。这样比较麻烦，最好双击便可执行此脚本。那么，怎么实现双击执行.sh文件呢？
##**第一步:修改权限**

首先，在脚本所在文件夹下打开终端，输入 chmod 777 script_name.sh (修改脚本权限，使其可执行)。
如图所示：
![1.png](https://upload-images.jianshu.io/upload_images/11501325-31443ef493b89381.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
##**第二步:修改打开方式**
进入脚本所在文件夹，点击左上角的Edit，点击Preferences（如图所示）
![2.jpg](https://upload-images.jianshu.io/upload_images/11501325-ec02d0cda4a03cad.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
出现如图所示窗口：
![3.png](https://upload-images.jianshu.io/upload_images/11501325-df2f8255768a89c4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
然后点击Behavior，在Executable Text Files选项中选择Run executable text files when they are opened。

**即可双击执行**

----------
**BUT**
双击执行.sh文件时，只会执行此种语句（我理解是，双击执行的是exec bash）
```
gnome-terminal -x bash -c "source /opt/ros/kinetic/setup.bash;roscore;exec bash"
```
对于下面这种（没有exec bash）语句并不执行
```
echo "\033[33m if you want to start perception or control or both,please enter 1 or 2 or 3 \033[0m"
read -p "press some key ,then press return :" KEY
case $KEY in 
```
这就导致，双击车上的ziway_mapapplication.sh，不会出现选择执行哪个模块的界面，而是全部执行！

----------
#那么如何解决？
我的做法是再写一个脚本，用来打开我们需要执行的那个脚本。

----------
在此，以我电脑上的为例进行讲解。
> * 需要执行的脚本名称：prediction.h
> * 需要执行的脚本的位置：/home/Desktop/RealVehicleTest
> * 新建脚本名称：exec.sh
> * 新建脚本位置：/home/Desktop

在桌面新建一个名为exec.sh的脚本，将其根据上述方法设为双击执行。
然后在文件中输入如下命令：
```
cd RealVehicleTest
if [ "$?" -eq "0" ];then
	echo "\033[33m cd RealVehicleTest ok~! \033[0m"
else
	echo "\033[31m Error: cd RealVehicleTest failed~! \033[0m"
              exit 1
fi
#start
gnome-terminal -x bash -c "source ~/Desktop/RealVehicleTest/prediction.bash;./prediction.sh;exec bash"
```
＃start之前的命令是进入脚本所在文件夹。
＃start下面的命令是执行prediction.sh
> source 路径;在终端执行的命令;执行bash

这样就可以实现双击桌面的exec.sh，运行prediction.sh。
水平有限，不足之处，请批评指正！