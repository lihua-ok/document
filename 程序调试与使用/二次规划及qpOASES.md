## [qpoases](https://projects.coin-or.org/qpOASES)
author: QiYao
这个库函数本来被设计来作为MPC的应用，但是也是一个可靠的QP算法求解方案。作为求解参数二次规划的有效集算法。
[使用说明书](https://www.coin-or.org/qpOASES/doc/3.2/manual.pdf)
### QP问题
#### 二次规划问题
带有二次型目标函数和约束条件的最优化问题。
##### [基础概念](http://blog.csdn.net/jbb0523/article/details/50598523)
二次型：函数中最高次为2次的函数。用矩阵可以记为$f=x^T \cdot A \cdot x$;
正定矩阵 Positive Definite Matrix：，设在二次型$f=x^T \cdot A \cdot x$中，对于任何$x\ne0$,都有$f(x)>0$,称$f$为正定二次型，称对称矩阵A为正定的;如果对于任何$x\ne0$,都有$f(x)<0$,称$f$为负定二次型，称对称矩阵A为负定的。
Hesse矩阵（海塞矩阵）：常用于牛顿法解决最优化问题。是一个类似与雅可比矩阵的概念，不过其是二阶导数的矩阵，但是雅可比矩阵是一阶导数的矩阵。如果函数f是连续的，则它的Hesse矩阵一定是对称阵。 得到函数f的Hesse矩阵有什么用呢？Hesse可以用于多元函数极值的判定。
[在清华的课程中也讲到了二次规划的问题](http://mathexp.math.tsinghua.edu.cn/optiweb/review/2_5qpam.htm)：
![QP-define.png](https://upload-images.jianshu.io/upload_images/11020056-51f74e99b101e037.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-matlab.png](https://upload-images.jianshu.io/upload_images/11020056-3c02f04ee56a6ee8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![QP-solution.png](https://upload-images.jianshu.io/upload_images/11020056-a8fc08cfad7251b9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 使用qpOASES求解步骤
以下文档都是从3.2版本的用户说明书中提取的简要部分，如果需要详细的还请阅读说明书：
1. 首先实例化一个QProblrm类的对象
`QProblem( int_t nV, int_t nC );` nv表示变量的个数，nc表示二次约束的的个数。
ex:`QProblem example( nV,nC );`
2. 初始化对象求求解。
![QP-init.png](https://upload-images.jianshu.io/upload_images/11020056-f0a577f3a1083ec4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
H：hessian矩阵  $H\in\mathbf{R^{nV \times nV}}$
g：梯度向量？$g\in\mathbf{R^{nV}}$
A：约束矩阵 $A\in\mathbf{R^{nC \times nV}}$
lb ub：上下限边界向量 $lb,ub\in\mathbf{R^{nV}}$
lbA ubA：上下限约束向量 $lb,ub\in\mathbf{R^{nC}}$。 **这个向量中不仅包含了不等式，也包括了等式的约束**,将上限和下限设置为相同数值即可表示为等式约束！
nWSR： 最大迭代次数
cputime：如果输入不为空，就会输出整个初始化和求解所花的时间。
如果那一项是没有的，就看可以传入一个空指针。解完后所有的向量需要自己释放内存。但是矩阵H A不需要。
### 使用qpOASES求解方式
QP问题分为许多种，有一些没有等式约束项，一些没有不等式约束项。因此在qpOASES中也有好几个方法来解决不同的问题，相比之下针对不同问题选择不同方法的效率会更高：

![QP-solving-variants.png](https://upload-images.jianshu.io/upload_images/11020056-90e24d0ebe3cfd65.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
针对不同的种类的详细用法可以参考`/examples/example**.cpp`
同时这个库可以解决线性规划，但是效率极低！
### qpOASES其余设置
>* 可以返回各项参数的数值
>* 可以定义 `Options myOptions;  qp.setOptions( myOptions );`设定需要设置的各参数
>* ex: ```
Options myOptions;
myOptions.setToMPC( );
myOptions.printLevel = PL_LOW;
qp.setOptions( myOptions );
```
>* 可以通过指定用于评估约束的函数来提升速度，但是可能这是naive的
>* 能够设置最大时间限制。因此由于时间测量的不准确，实际的总CPU时间可能比允许的CPU时间稍高一些。
