﻿# Prediction评估器解析
**作者：谢枫**
标签（空格分隔）： 分析

---
##作用
行为预测的轨迹，既包括了障碍物在将来一段时间内运动的方向，还体现了它们在运动中的速度变化。譬如行人过马路的时候会预测他们使用较为恒定的步行速度，车辆转弯的时候会先减速后加速，而加减速的快慢也取决于弯道的弧度和长短。所以可以将障碍物的行为预测抽象成宏观层面的行为预测和轨迹生成两个问题来解决。

宏观层面的行为预测问题，往往可以抽象成经典的机器学习问题，并且利用基于大数据的深度学习技术来解决。例如， 在假设车辆按照高精地图划分的道路（Lane）行驶的前提下，我们可以认为在任何一个时刻，车辆可行驶的每一个Lane序列都是一个需要进binary classification的样本。在这个假设下，我们不需要对直行、并道、路口拐弯等场景进行区分处理，因为无论是直行、并道，和路口拐弯，都可以统一看成是车辆在不同Lane序列上的行驶。

如下图所示，在t时刻，障碍物车辆位于Lane 1，此时按照Lane序列的可能展开途径，我们考虑三个车道序列：
![sequence.png](https://upload-images.jianshu.io/upload_images/11501325-1638267ea1f267a9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
Lane Sequence 1： Lane 1、Lane 2、Lane 3对应路口右转；
Lane Sequence 2：Lane 1、Lane 6、Lane 8对应路口直行；
Lane Sequence 3：Lane 1、Lane 4、Lane 5、Lane7对应换道后直行通过路口。

评估器的作用就在于，依据训练好的神经网络模型，得到障碍物行驶不同车道序列的概率。

Apollo中目前设定了三种评估器，分别为**Cost Evaluator、MLP Evaluator、RNN Evaluator**。
##Cost Evaluator
通过一组成本函数计算得到概率，主要用于高速公路导航模式下。
##MLP Evaluator
###感知机：PLA
多层感知机是由感知机推广而来，感知机学习算法(PLA: Perceptron Learning Algorithm)用神经元的结构进行描述的话就是一个单独的。

感知机的神经网络表示如下： 
![4.png](https://upload-images.jianshu.io/upload_images/11501325-90a5dc284f39183c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
<center>![function1.png](https://upload-images.jianshu.io/upload_images/11501325-5565ddef69c2385f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)</center>
从上述内容更可以看出，PLA是一个线性的二分类器，但不能对非线性的数据并不能进行有效的分类。因此便有了对网络层次的加深，理论上，多层网络可以模拟任何复杂的函数。
###多层感知机：MLP
MLP为多层感知机，属于一种较为简单的神经网络模型。
下图为MLP的网络结构：
![6.png](https://upload-images.jianshu.io/upload_images/11501325-e4ec228ba4dd2f20.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
多层感知机由感知机推广而来，最主要的特点是有多个神经元层，因此也叫深度神经网络(DNN: Deep Neural Networks)。
我们将第一层称之为输入层，最后一层称之为输出层，中间的层称之为隐藏层。MLP并没有规定隐层的数量，因此可以根据各自的需求选择合适的隐层层数。且对于输出层神经元的个数也没有限制。
计算时，将障碍物的特征值和车道特征值作为神经元的输入，模型通过计算得到输出即概率。
##RNN Evaluator
人类并不是每时每刻都从一片空白的大脑开始他们的思考。在你阅读这篇文章时候，你都是基于自己已经拥有的对先前所见词的理解来推断当前词的真实含义。我们不会将所有的东西都全部丢弃，然后用空白的大脑进行思考。我们的思想拥有持久性。

传统的神经网络并不能做到这点，看起来也像是一种巨大的弊端。例如，假设你希望对电影中的每个时间点的时间类型进行分类。传统的神经网络应该很难来处理这个问题——使用电影中先前的事件推断后续的事件。

RNN 解决了这个问题。RNN 是包含循环的网络，允许信息的持久化。
![7.png](https://upload-images.jianshu.io/upload_images/11501325-13e22061000bd7d8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
RNN 包含循环
在上面的示例图中，神经网络的模块，A，正在读取某个输入 x_i，并输出一个值 h_i。循环可以使得信息可以从当前步传递到下一步。这些循环使得 RNN 看起来非常神秘。然而，如果你仔细想想，这样也不比一个正常的神经网络难于理解。RNN 可以被看做是同一神经网络的多次赋值，每个神经网络模块会把消息传递给下一个。所以，如果我们将这个循环展开：
![8.png](https://upload-images.jianshu.io/upload_images/11501325-93e2dd95a424c29a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
展开的 RNN
链式的特征揭示了 RNN 本质上是与序列和列表相关的。他们是对于这类数据的最自然的神经网络架构。
所以在预测模块加入了RNN，结合障碍物的历史轨迹进行评估。
具体参考链接如下：
[RNN分析参考链接][1]
  [1]: https://blog.csdn.net/prom1201/article/details/52221822
